# -*- coding: utf-8 -*-
{
    'name': 'Saasmate Portal',
    'version': '1.0',
    'category': 'General',
    'sequence': 3,
    'summary': 'Amtech ERP System',
    'author': 'Pragmatic Techsoft Pvt. Ltd.',
    'website': 'www.pragtech.co.in',
    'description': """
ERP System for Amtech
====================

Provide access for Claims and Back orders to the existing and newly developed E-commerce system. 

""",
    'depends': ['crm_claim','website_portal','website_portal_sale','stock'],
    'data': [
             'views/amtech_portal_crm_claim_template.xml'
    ],
    'demo': [],
    'test': [],
    'installable': True,
    'auto_install': False,
    'application': True,
}
