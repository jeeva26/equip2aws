from datetime import datetime
from odoo import api, fields, models, tools, _
import logging
_logger = logging.getLogger(__name__)

class StockPicking(models.Model):
    _inherit = "stock.picking"
    
    @api.model
    def create(self, vals):
        res = super(StockPicking, self).create(vals)
        for picking in res:
            if picking.partner_id not in picking.message_partner_ids and picking.picking_type_id.code != 'internal':
                picking.message_subscribe([picking.partner_id.id])        
        return res        