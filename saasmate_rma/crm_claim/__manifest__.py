# -*- coding: utf-8 -*-

{
    'name': 'Claims Management',
    'version': '1.0',
    'category': 'Sales',
    'author': 'Pragmatic Techsoft Pvt. ltd.',
    'website': 'www.pragtech.co.in',
    'description': """

Manage Customer Claims.
=======================
This application allows you to track your customers/vendors claims and grievances.

It is fully integrated with the email gateway so that you can create
automatically new claims based on incoming emails.
    """,
    'depends': ['crm','sales_team','sale_stock'],
    'data': [
        'crm_claim_view.xml',
        'crm_claim_menu.xml',
        'security/ir.model.access.csv',
        'report/crm_claim_report_view.xml',
        'crm_claim_data.xml',
        'res_partner_view.xml',
        'report/claim_report_template.xml',
        'report/claim_report.xml',
        'data/mail_template_data.xml',
    ],
    'demo': ['crm_claim_demo.xml'],
    'test': [
        'test/process/claim.yml',
        'test/ui/claim_demo.yml'
    ],
    'installable': True,
    'auto_install': False,
}
