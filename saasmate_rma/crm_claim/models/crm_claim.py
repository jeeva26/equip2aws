# -*- coding: utf-8 -*-
from odoo.tools.translate import _
from odoo.tools import html2plaintext
from odoo import api, fields, models
from odoo.addons.base.res import res_request
from itertools import groupby
import datetime


class crm_claim_stage(models.Model):
    """ Model for claim stages. This models the main stages of a claim
        management flow. Main CRM objects (leads, opportunities, project
        issues, ...) will now use only stages, instead of state and stages.
        Stages are for example used to display the kanban view of records.
    """
    _name = "crm.claim.stage"
    _description = "Claim stages"
    _rec_name = 'name'
    _order = "sequence"

    name = fields.Char('Stage Name', required=True, translate=True)
    sequence = fields.Integer(
        'Sequence', help="Used to order stages. Lower is better.", default=lambda *args: 1)
    team_ids = fields.Many2many('crm.team', 'crm_team_claim_stage_rel', 'stage_id', 'team_id', string='Teams',
                                help="Link between stages and sales teams. When set, this limitate the current stage to the selected sales teams.")
    case_default = fields.Boolean('Common to All Teams',
                                  help="If you check this field, this stage will be proposed by default on each sales team. It will not assign this stage to existing teams.")


class crm_claim(models.Model):
    """ Crm claim
    """
    _name = "crm.claim"
    _description = "Claim"
    _order = "priority,date desc"
    _inherit = ['mail.thread']

    @api.model
    def _get_default_stage_id(self):
        """ Gives default stage_id """
        team_id = self.env['crm.team']._get_default_team_id(
            user_id=self._uid).id
        return self.stage_find([], team_id, [('sequence', '=', '1')])

    @api.model
    def referencable(self):
        return [(link.object, link.name) for link in self.env['res.request.link'].search([])]

    name = fields.Char('Claim Subject', required=True)
    active = fields.Boolean('Active', default=True)
    action_next = fields.Char('Next Action')
    date_action_next = fields.Datetime('Next Action Date')
    description = fields.Text('Description')
    resolution = fields.Text('Resolution')
    write_date = fields.Datetime('Update Date', readonly=True)
    date_deadline = fields.Date('Deadline')
    date_closed = fields.Datetime('Closed', readonly=True)
    date = fields.Datetime('Claim Date', default=datetime.datetime.now())
    ref = fields.Reference(string='Reference', selection=referencable)
    categ_id = fields.Many2one('crm.claim.category', 'Category')
    priority = fields.Selection(
        [('0', 'Low'), ('1', 'Normal'), ('2', 'High')], 'Priority', default='1')
    type_action = fields.Selection(
        [('correction', 'Corrective Action'), ('prevention', 'Preventive Action')], 'Action Type')
    user_id = fields.Many2one(
        'res.users', 'Responsible', track_visibility='always', default=lambda self: self.env.user)
    user_fault = fields.Char('Trouble Responsible')
    team_id = fields.Many2one('crm.team', 'Sales Team', oldname='section_id',
                              help="Responsible sales team."
                              " Define Responsible user and Email account for"
                              " mail gateway.", default=lambda self: self.env['crm.team'].sudo()._get_default_team_id(user_id=self.env.uid))
    company_id = fields.Many2one(
        'res.company', string='Company', required=True, index=True, default=lambda self: self.env.user.company_id)
    partner_id = fields.Many2one(
        'res.partner', 'Partner', track_visibility='always', change_default=True)
    email_cc = fields.Text('Watchers Emails', size=252,
                           help="These email addresses will be added to the CC field of all inbound and outbound emails for this record before being sent. Separate multiple email addresses with a comma")
    email_from = fields.Char(
        'Email', size=128, help="Destination email for email gateway.")
    partner_phone = fields.Char('Phone')
    stage_id = fields.Many2one(
        'crm.claim.stage', 'Stage', track_visibility='onchange', default=_get_default_stage_id)
    cause = fields.Text('Root Cause')
    layout_category_id = fields.Many2one(
        'sale.layout_category', string='Section')

    def stage_find(self, cases, team_id, domain=[], order='sequence'):
        """ Override of the base.stage method
            Parameter of the stage search taken from the lead:
            - team_id: if set, stages must belong to this team or
              be a default case
        """
        if isinstance(cases, (int, long)):
            cases = self.browse(cases)
        # collect all team_ids
        team_ids = []
        if team_id:
            team_ids.append(team_id)
        for claim in cases:
            if claim.team_id:
                team_ids.append(claim.team_id.id)
        # OR all team_ids and OR with case_default
        search_domain = []
        if team_ids:
            search_domain += [('|')] * len(team_ids)
            for team_id in team_ids:
                search_domain.append(('team_ids', '=', team_id))
        search_domain.append(('case_default', '=', True))
        # AND with the domain in parameter
        search_domain += list(domain)
        # perform search, return the first found
        stage_ids = [stages.id for stages in self.env[
            'crm.claim.stage'].search(search_domain, order=order)]
        if stage_ids:
            return stage_ids[0]
        return False

    @api.onchange('partner_id')
    def onchange_partner_id(self, partner_id, email=False, context=None):
        """This function returns value of partner address based on partner
           :param email: ignored
        """
        address = self.env['res.partner'].browse(partner_id)
        return {'value': {'email_from': address.email, 'partner_phone': address.phone or address.mobile}}

    @api.model
    def create(self, vals):
        context = dict(self._context or {})
        if vals.get('team_id') and not context.get('default_team_id'):
            context['default_team_id'] = vals.get('team_id')
        # context: no_log, because subtype already handle this
        return super(crm_claim, self).create(vals)

    @api.multi
    def copy(self, default=None):
        self.ensure_one()
        default = dict(default or {},
                       stage_id=self._get_default_stage_id(),
                       name=_('%s (copy)') % self.name)
        return super(crm_claim, self).copy(default=default)

    # -------------------------------------------------------
    # Mail gateway
    # -------------------------------------------------------
    @api.multi
    def message_new(self, msg, custom_values=None):
        """ Overrides mail_thread message_new that is called by the mailgateway
            through message_process.
            This override updates the document according to the email.
        """
        if custom_values is None:
            custom_values = {}
        desc = html2plaintext(msg.get('body')) if msg.get('body') else ''
        defaults = {
            'name': msg.get('subject') or _("No Subject"),
            'description': desc,
            'email_from': msg.get('from'),
            'email_cc': msg.get('cc'),
            'partner_id': msg.get('author_id', False),
        }
        if msg.get('priority'):
            defaults['priority'] = msg.get('priority')
        defaults.update(custom_values)
        return super(crm_claim, self).message_new(msg, custom_values=defaults)

    @api.multi
    def claim_lines_layouted(self):
        """
        Returns this order lines classified by sale_layout_category and separated in
        pages according to the category pagebreaks. Used to render the report.
        """
        self.ensure_one()
        report_pages = [[]]
        for category, lines in groupby(self.claim_line_ids, lambda l: l.layout_category_id):
            # If last added category induced a pagebreak, this one will be on a
            # new page
            if report_pages[-1] and report_pages[-1][-1]['pagebreak']:
                report_pages.append([])
            # Append category to current report page
            report_pages[-1].append({
                'name': category and category.name or 'Uncategorized',
                'subtotal': category and category.subtotal,
                'pagebreak': category and category.pagebreak,
                'lines': list(lines)
            })

        return report_pages
    
    @api.multi
    def action_claim_send(self):
        '''
        This function opens a window to compose an email, with the edi Claim template message loaded by default
        '''
        self.ensure_one()
        ir_model_data = self.env['ir.model.data']
        try:
            template_id = ir_model_data.get_object_reference('crm_claim', 'email_template_edi_claim')[1]
        except ValueError:
            template_id = False
        try:
            compose_form_id = ir_model_data.get_object_reference('mail', 'email_compose_message_wizard_form')[1]
        except ValueError:
            compose_form_id = False
        ctx = dict()
        ctx.update({
            'default_model': 'crm.claim',
            'default_res_id': self.ids[0],
            'default_use_template': bool(template_id),
            'default_template_id': template_id,
            'default_composition_mode': 'comment',
        })
        return {
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'mail.compose.message',
            'views': [(compose_form_id, 'form')],
            'view_id': compose_form_id,
            'target': 'new',
            'context': ctx,
        }     


class res_partner(models.Model):
    _inherit = 'res.partner'

    @api.one
    def _claim_count(self):
        claim_id = self.env['crm.claim']
        count = claim_id.search_count(
            ['|', ('partner_id', 'in', self.child_ids.ids), ('partner_id', '=', self.id)])
        self.claim_count = count

    claim_count = fields.Integer(compute='_claim_count', string='# Claims')


class crm_claim_category(models.Model):
    _name = "crm.claim.category"
    _description = "Category of claim"

    name = fields.Char('Name', required=True, translate=True)
    team_id = fields.Many2one('crm.team', 'Sales Team')
