# -*- coding: utf-8 -*-

{'name': 'RMA Claims Mass Return by Lot',
 'category': 'Generic Modules/CRM & SRM',
 'depends': ['crm_claim_rma'
             ],
 'author': 'Pragmatic Techsoft Pvt. ltd.',
 'website': 'www.pragtech.co.in',
 'version': '1.0',
 'description': """
RMA Claim Mass Return by Lot
============================

This module adds possibility to return a whole lot of product from a Claim
and create a incoming shipment for them.


WARNING: This module is currently not yet completely debugged and is waiting his author to be.

""",
 'images': [],
 'demo': [],
 'data': [
        'wizard/returned_lines_from_serial_wizard_view.xml',
        'crm_rma_view.xml',
 ],
 'installable': True,
 'application': True,
}
