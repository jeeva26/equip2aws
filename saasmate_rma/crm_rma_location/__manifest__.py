# -*- coding: utf-8 -*-
{
    'name': 'RMA Location',
    'version': '1.0.0',
    'author': 'Pragmatic Techsoft Pvt. ltd.',
    'website': 'www.pragtech.co.in',
    'category': 'Generic Modules/CRM & SRM',
    'depends': [
        'stock',
        'procurement',
    ],
    'data': [
        'views/stock_warehouse.xml',
    ],
    'post_init_hook': 'post_init_hook',
    'installable': True,
    'auto_install': False,
}
