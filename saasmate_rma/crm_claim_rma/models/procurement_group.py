# -*- coding: utf-8 -*-
from openerp import fields, models

class ProcurementGroup(models.Model):
    _inherit = 'procurement.group'

    claim_id = fields.Many2one('crm.claim', 'Claim')
