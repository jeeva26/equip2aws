# -*- coding: utf-8 -*-
from odoo import api, fields, models

class StockPicking(models.Model):

    _inherit = "stock.picking"

    claim_id = fields.Many2one('crm.claim', 'Claim')
    
    @api.model
    def create(self, vals):
        if self._context.get('picking_type') == 'out':
            if self._context.get('claim_id'):
                vals.update({'claim_id': self._context.get('claim_id')})
            if self._context.get('partner_id'):
                vals.update({'partner_id': self._context.get('partner_id')})
        res = super(StockPicking, self).create(vals)
#         if not res.claim_id and self._context.get('claim_id'):
#             res.claim_id = self._context.get('claim_id')
        return res
