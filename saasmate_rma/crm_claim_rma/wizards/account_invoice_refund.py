# -*- coding: utf-8 -*-
from odoo import api, fields, models

class AccountInvoiceRefund(models.TransientModel):
    _inherit = "account.invoice.refund"

    def _default_description(self):
        return self.env.context.get('description', '')
 
    #overrride this two fields to change the description and to avoid 'Record does not exist warning'
    description = fields.Char(string='Reason', required=True,default=_default_description)
    refund_only = fields.Boolean(string='Technical field to hide filter_refund in case invoice is partially paid', compute='_get_refund_only')
    
    @api.depends('date_invoice')
    @api.one
    def _get_refund_only(self):
        invoice_ids = self.env.context.get('invoice_ids', [])
        if invoice_ids:
            self = self.with_context(active_ids=invoice_ids,active_id=invoice_ids[0])
        invoice_id = self.env['account.invoice'].browse(self._context.get('active_id',False))
        if len(invoice_id.payment_move_line_ids) != 0 and invoice_id.state != 'paid':
            self.refund_only = True
        else:
            self.refund_only = False
            
    @api.multi
    def compute_refund(self, mode='refund'):
        self.ensure_one()
        invoice_ids = self.env.context.get('invoice_ids', [])
        if invoice_ids:
            self = self.with_context(active_ids=invoice_ids)
        return super(AccountInvoiceRefund, self).compute_refund(mode=mode)
