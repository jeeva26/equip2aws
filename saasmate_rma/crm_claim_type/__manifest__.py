# -*- coding: utf-8 -*-
{
    'name': 'CRM Claim Types',
    'category': 'Customer Relationship Management',
    'summary': 'Claim types for CRM',
    'author': 'Pragmatic Techsoft Pvt. ltd.',
    'website': 'www.pragtech.co.in',
    'version': '1.0.0',
    'description': """
RMA System for Amtech
====================
""",    
    'depends': [
        'crm_claim',
    ],
    'data': [
        'data/crm_claim_type.xml',
        'data/crm_claim_stage.xml',
        'security/ir.model.access.csv',
        'views/crm_claim.xml',
        'views/crm_claim_stage.xml',
        'views/crm_claim_type.xml',
    ],
    'demo': [
        'demo/crm_claim.xml',
        'demo/crm_claim_stage.xml',
    ],
    'installable': True,
    'auto_install': False,
}
