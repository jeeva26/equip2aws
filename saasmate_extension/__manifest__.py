# -*- coding: utf-8 -*-
{
    'name': 'Saasmate Extension',
    'version': '1.0',
    'category': 'General',
    'sequence': 3,
    'summary': 'Saasmate ERP System',
    'author': 'Pragmatic Techsoft Pvt. Ltd.',
    'website': 'http://pragtech.co.in',
    'description': """
ERP System for Saasmate
====================

Provide access for products, customers and price lists to the existing and newly developed E-commerce system. 

""",
    'depends': ['website_sale','purchase','mrp','account','stock','sale_margin'],
    'data': [
        'data/product_data.xml',
        'data/account_data.xml',
        'data/template_data.xml',
        'data/partner_classification_data.xml',
#         'security/product_security.xml',
        'security/ir.model.access.csv',
        'views/amtech_cron_jobs.xml',
        'views/product_views.xml',
        'views/partner_view.xml',
        'views/sale_view.xml',
        'views/mrp_bom_views.xml',
        'views/amtech_reports.xml',
        'views/report_pricelist_percentage_increase.xml',
        'views/stock_view.xml',
        'views/website_templates.xml',
        'views/account_view.xml',
        'views/amtech_views.xml',
        'report/sale_analysis.xml',
    ],
    'demo': [],
    'test': [],
    'installable': True,
    'auto_install': False,
    'application': True,
}
