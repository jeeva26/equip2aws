# from odoo import api, models
# 
# class View(models.Model):
#     _inherit = 'ir.ui.view'
#     
#     @api.depends('arch_db', 'arch_fs')
#     def _compute_arch(self):
#         '''Provide filter records for dynamically created journal types of bank and cash'''
#         res = super(View,self)._compute_arch()
#         bank_cash_ids = self.env['account.journal'].sudo().search([('type','in',['bank','cash'])])
#         
#         filters = ''''''
#         for each_journal in bank_cash_ids:
#             filters += '''<filter name="journal_'''+str(each_journal.id)+'''" string="'''+each_journal.name+'''" domain="[('account_journal_id','=','''+str(each_journal.id)+''')]"/>'''
#         for view in self:
#             if view.name == 'account.invoice.groupby.inherit':
#                 view.arch_db = '''<?xml version="1.0"?>
#                 <xpath expr="//filter[@name='refunds']" position="after">
#                     <separator/>
#                     '''+filters+'''
#                 </xpath>'''
#                 view.arch = view.arch_db
#         return res