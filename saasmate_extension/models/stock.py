# -*- coding: utf-8 -*-

from datetime import datetime
from odoo import api, fields, models, tools, _
import logging
_logger = logging.getLogger(__name__)

class StockPicking(models.Model):
    _inherit = "stock.picking"
    
#     report_template_id = fields.Many2one('report.templates','Template')
    
    def _get_days_to_complete(self):
        '''Calculate days to complete for DIFOT'''
        for line in self:
            if line.max_date and not line.date_done:
                max_date = datetime.strptime(line.max_date, '%Y-%m-%d %H:%M:%S')
                line.days_to_complete = (max_date - datetime.today()).days
            elif line.max_date and line.date_done:
                min_date = datetime.strptime(line.min_date, '%Y-%m-%d %H:%M:%S')
                date_done = datetime.strptime(line.date_done, '%Y-%m-%d %H:%M:%S')
                line.days_to_complete = (min_date - date_done).days
            
    days_to_complete = fields.Float('Delay Days',compute = "_get_days_to_complete")
    
    def _create_lots_for_picking(self):
        '''Override create lots on stock picking validate to add expiry date fields in lot'''
        Lot = self.env['stock.production.lot']
        for pack_op_lot in self.mapped('pack_operation_ids').mapped('pack_lot_ids'):
            if not pack_op_lot.lot_id:
                lot = Lot.create({'name': pack_op_lot.lot_name, 'product_id': pack_op_lot.operation_id.product_id.id, 'life_date': pack_op_lot.expiry_date,'use_date':pack_op_lot.use_date,'removal_date':pack_op_lot.removal_date,'alert_date':pack_op_lot.alert_date})
                pack_op_lot.write({'lot_id': lot.id})
        # TDE FIXME: this should not be done here
        self.mapped('pack_operation_ids').mapped('pack_lot_ids').filtered(lambda op_lot: op_lot.qty == 0.0).unlink()
    create_lots_for_picking = _create_lots_for_picking
    
    @api.model
    def create(self, vals):
        """Set sale order report template in picking as well to print company logo on packing slip and picking list report """
        if 'origin' in vals:
            order = self.env['sale.order'].search([('name','=',vals.get('origin'))],limit=1)
#             if order:
#                 vals.update({'report_template_id':order.report_template_id.id})
        return super(StockPicking,self).create(vals)
    
class StockPackOperationLot(models.Model):
    _inherit = "stock.pack.operation.lot"
    
    '''Expiry date fields added to get dates when creating lots'''
    expiry_date = fields.Date('Expiry Date')
    use_date = fields.Date('Best Before Date')
    alert_date = fields.Date('Alert Date')
    removal_date = fields.Date('Removal Date')
    
    
class StockQuant(models.Model):
    _inherit = 'stock.quant'
    
    """add expiry date in 'Inventory Valuation' analysis view"""
    life_date = fields.Datetime(string='Expiry Date', related='lot_id.life_date', store=True)
    
    @api.model
    def create(self,vals):
        res = super(StockQuant, self).create(vals)
        
        #product cost price update by using average costing base on inventory valuation
        tot_inventory_value = 0.0
        tot_qty = 0
        for quant in self.search([('product_id','=',res.product_id.id)]):
            tot_inventory_value += quant.inventory_value  
            tot_qty += quant.qty
        if tot_inventory_value and tot_qty:
            standard_price = tot_inventory_value/tot_qty
            res.product_id.standard_price = standard_price
            
        return res