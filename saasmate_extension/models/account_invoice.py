# -*- coding: utf-8 -*-

from odoo import api, fields, models, tools, _
from odoo.exceptions import ValidationError, except_orm
import logging

_logger = logging.getLogger(__name__)

class AccountInvoice(models.Model):
    _inherit="account.invoice"
    
    @api.model
    def _cron_send_validate_invoices_email(self):
        """Check for open state and not sent invoices at regular basis and send that invoices via email."""
        acc_invoice = self.search([('state','=','open'),('sent','=',False)])
        for invoice in acc_invoice:
            invoice.send_invoice_mail()
    
    @api.one
    def send_invoice_mail(self):
        """Create an invoice email template and send to partner also log this below invoice object chatter"""
        self.ensure_one()
        email_act = self.action_invoice_sent()
        if email_act and email_act.get('context'):
            email_ctx = email_act['context']
            self.with_context(email_ctx).message_post_with_template(email_ctx.get('default_template_id'))

#     report_template_id = fields.Many2one('report.templates','Template')
    #Related to res.partner payment type. Used to add filter at invoice level
#     account_journal_id = fields.Many2one(related='partner_id.account_journal_id', string="Payment Type", store="True")
    account_journal_id = fields.Selection([('direct_credit','Direct Credit'),('other','Other')],related='partner_id.account_journal_id', string="Payment Type", store="True")
AccountInvoice()