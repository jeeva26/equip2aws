# -*- coding: utf-8 -*-

from odoo import api, fields, models, tools, _
from odoo.exceptions import ValidationError, except_orm
import logging

_logger = logging.getLogger(__name__)


class MrpBomLine(models.Model):
    _inherit = "mrp.bom.line"
    
    on_hand_qty = fields.Float("On Hand Qty", related="product_id.qty_available", help="Product on hand quantity")
    ordered_qty = fields.Float("Ordered Qty", related="product_id.incoming_qty", help="Product incoming quantity")
    product_tmpl_id = fields.Many2one("product.template","Product template reference", related="bom_id.product_tmpl_id", help="Used to view BOM component on product template form view")
    po_arrival_date = fields.Datetime('PO Arrival Date', compute='_get_recent_po_arrival_date', help="Recent purchase order arrival date")
    
    def _get_recent_po_arrival_date(self):
        for bom_line in self:
            arrival_date = bom_line.get_recent_po_arrival_date()
            bom_line.po_arrival_date = arrival_date[0] if isinstance(arrival_date, list) else arrival_date
    
    @api.one
    def get_recent_po_arrival_date(self):
        """Returns recent purchase order arrival date for BOM line level product."""
        self.ensure_one()
        product_id = self.product_id.id
        arrival_date = ''
        po_lst = self.env['purchase.order'].search([('state','=','purchase'),('order_line.product_id','=',product_id)])
        for po in po_lst:
            if not arrival_date:
                arrival_date = po.date_planned
                continue
            if po.date_planned < arrival_date:
                arrival_date = po.date_planned
        return arrival_date
MrpBomLine()