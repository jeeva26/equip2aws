# -*- coding: utf-8 -*-

from . import product,partner
from . import sale
from . import mrp_bom
from . import account_invoice
from . import stock
from . import res_users
from . import templates
# from . import inherited_sale_line
# from . import ir_view