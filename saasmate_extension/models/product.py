# -*- coding: utf-8 -*-

from odoo import api, fields, models, tools, _
from odoo.exceptions import ValidationError, except_orm
from odoo.addons import decimal_precision as dp
from odoo.tools.float_utils import float_round
import logging
from odoo import SUPERUSER_ID

_logger = logging.getLogger(__name__)

class Features(models.Model):
    _name='product.features'
     
    name = fields.Char('Feature', help="Product Features")
    display_on_report=fields.Boolean('Display On Report')
    product_id=fields.Many2one('product.template','product')
#     product_template_import_stage_id=fields.Many2one('migration.product.template.import.stage','Product Template Import Stage')

class ProductTemplate(models.Model):
    _inherit = "product.template"
    
    features = fields.Html('Features', help="Product Features")
    feature_ids=fields.One2many('product.features','product_id','Features')
    benefits = fields.Html('Benefits', help='Product benefits')
    carton_qty = fields.Float('Carton Qty', help="Carton Qty")
    pack_qty = fields.Float('Pack Qty', help="Pack Qty")
    is_clearence = fields.Boolean('Product Clearence', help="Product Clearence")
    is_website_use = fields.Boolean('Is Website Use', help='This field is used by "Convergence"')
    bom_line_ids = fields.One2many('mrp.bom.line', 'product_tmpl_id', 'BoM Lines', help="Bom components reference linked to product template")
    available_for_sale = fields.Float('Available for Sale', compute='_compute_quantities',
        digits=dp.get_precision('Product Unit of Measure'),help="Available for sales = Stock on hand – existing order")
#     categ_id = fields.Many2one(
#         'product.category', 'Internal Category',
#         change_default=True, default=_get_default_category_id, domain="[('type','=','normal')]",
#         required=True, help="Select category for the current product")   
    categ_id = fields.Many2one(
        'product.category', 'Internal Category',
        change_default=True, domain="[('type','=','normal')]",
        required=True, help="Select category for the current product") 
    
    
    @api.one
    def action_available_for_sale(self):
        return True
    
    def _compute_quantities(self):
        res = self._compute_quantities_dict()
        for template in self:
            template.qty_available = res[template.id]['qty_available']
            template.virtual_available = res[template.id]['virtual_available']
            template.incoming_qty = res[template.id]['incoming_qty']
            template.outgoing_qty = res[template.id]['outgoing_qty']
            template.available_for_sale = res[template.id]['available_for_sale']
    
    def _compute_quantities_dict(self):
        res = super(ProductTemplate, self)._compute_quantities_dict()
        for template_id in res:
            res[template_id]['available_for_sale'] = res[template_id]['qty_available'] - res[template_id]['outgoing_qty']
        return res
ProductTemplate()

class ProductProduct(models.Model):
    _inherit = "product.product"

    ## Fields
    available_for_sale = fields.Float('Available for Sale', compute='_compute_quantities',
        digits=dp.get_precision('Product Unit of Measure'),help="Available for sales = Stock on hand – existing order")
    
    @api.one
    def action_available_for_sale(self):
        return True

    #This method is override to add quantity available for sale
    @api.depends('stock_quant_ids', 'stock_move_ids')
    def _compute_quantities(self):
        res = self._compute_quantities_dict(self._context.get('lot_id'), self._context.get('owner_id'), self._context.get('package_id'), self._context.get('from_date'), self._context.get('to_date'))
        for product in self:
            product.qty_available = res[product.id]['qty_available']
            product.incoming_qty = res[product.id]['incoming_qty']
            product.outgoing_qty = res[product.id]['outgoing_qty']
            product.virtual_available = res[product.id]['virtual_available']
            product.available_for_sale = res[product.id]['available_for_sale']
    #Will compute available for sale
    @api.multi
    def _compute_quantities_dict(self, lot_id, owner_id, package_id, from_date=False, to_date=False):
        res = super(ProductProduct, self)._compute_quantities_dict(lot_id, owner_id, package_id, from_date=from_date, to_date=to_date)
        for product_id in res:
            res[product_id]['available_for_sale'] = float_round(
                res[product_id]['qty_available'] - res[product_id]['outgoing_qty'],
                precision_rounding=self.browse(product_id).uom_id.rounding)
        return res
    
ProductProduct()

class ProductSupplierInfo(models.Model):
    _inherit = "product.supplierinfo"
    _rec_name = "product_code" #Supplier product code searcheable in product template view 

ProductSupplierInfo()

class ProductPricelist(models.Model):
    _inherit = "product.pricelist"
    
    percent_increase = fields.Float('Increase %', help="Increase % of pricelist with respective previous pricelist")
    date_of_increase = fields.Date('Increase % date', default=fields.Date.context_today, help='Increase % date')
    
ProductPricelist()