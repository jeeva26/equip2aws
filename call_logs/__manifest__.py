{
    "name": "Maintain Call Logs",
    "version": "1.0",
    "author": "Pragmatic Techsoft Pvt. Ltd.",
    "category": "CRM",
    "description": """

========================================

""",
    "website": "www.pragtech.co.in",
    "depends": ["crm",
                ],
    "data": [
        'security/ir.model.access.csv',
        'views/web_js_change.xml',
        'views/crm_view.xml',
        'views/partner_view.xml',
    ],
    "installable": True,
    "active": False,
}
