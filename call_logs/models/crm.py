from odoo import fields,models,api, _
# from bzrlib.osutils import lstat

class CrmActivityLog(models.TransientModel):
    _inherit = "crm.activity.log"

    @api.multi
    def action_log_and_schedule(self):
        self.ensure_one()
        self.action_log()
        view_id = self.env.ref('crm.crm_activity_log_view_form_schedule')
        return {
            'name': _('Next activity'),
            'res_model': 'crm.activity.log',
            'context': {
                'default_last_activity_id': self.next_activity_id.id,
                'default_lead_id': self.lead_id.id,
                'default_partner_id':self.partner_id.id
            },
            'type': 'ir.actions.act_window',
            'view_id': False,
            'views': [(view_id.id, 'form')],
            'view_mode': 'form',
            'target': 'new',
            'view_type': 'form',
            'res_id': False
        }


    @api.onchange('opportunity_id')
    def onchange_opportunity_id(self):
        opp_ids=[]
        opp_obj = self.env['crm.lead'].search([('type','=','opportunity'),('partner_id','!=',False),('partner_id','=',self._context['active_id'])])
        if opp_obj:
            for opp in opp_obj:
                opp_ids.append(opp.id)
        return {'domain': {'opportunity_id': [('id', '=', opp_ids)]}}
   
    @api.model
    def _default_lead_id(self):
        if 'default_lead_id' in self._context:
            return self._context['default_lead_id']
        if self._context.get('active_model') == 'crm.lead':
            return self._context.get('active_id')
        return False
    
    lead_id = fields.Many2one('crm.lead', 'Lead', required=False, default=_default_lead_id)
    status = fields.Selection([('to_do','To Do'),('done','Done')])
    flag = fields.Boolean('Flag',help="True if created default for scheduling call", default=False)
    partner_id = fields.Many2one('res.partner', "Partner ID")
    opportunity_id = fields.Many2one('crm.lead', 'Opportunity',) 
    category_id = fields.Many2one('activity.call.log', "Category")
    call_flag = fields.Boolean(string='Call Flag', default=False, help="True if activity type is call")
    custom_duration = fields.Float(default=0,string="Call Summary (min/s)")
    
    @api.onchange('lead_id')
    def onchange_lead_id(self):
#         self.next_activity_id = self.lead_id.next_activity_id
        self.date_deadline = self.lead_id.date_deadline
        self.team_id = self.lead_id.team_id
        self.planned_revenue = self.lead_id.planned_revenue
        self.title_action = self.lead_id.title_action

    @api.onchange('recommended_activity_id')
    def onchange_recommended_activity_id(self):
        self.next_activity_id = 2
    
    @api.model
    def default_get(self, fields_list):
        res = super(CrmActivityLog,self).default_get(fields_list)
        call = self.env.ref('crm.crm_activity_data_call')
        if 'btn_name' in self._context:
            if str(self._context['btn_name'])=="logcall":
                res.update({'next_activity_id':call.id})
            elif str(self._context['btn_name'])=="schedulecall":
                res.update({'next_activity_id':call.id})
        return res
#     
    
    def change_state(self):
        self.write({'status':"done"})
        
    @api.multi
    def action_schedule(self):
        if self.lead_id:
            self.partner_id = self.lead_id.partner_id.id
            self.lead_id.write({'planned_revenue': self.planned_revenue,})
        else:
            self.partner_id=self.partner_id
        res = super(CrmActivityLog,self).action_schedule()
        self.status = "to_do"
        #set call flag to true
        call_activity_id = self.env.ref('crm.crm_activity_data_call')
        if call_activity_id.id == self.next_activity_id.id:
            self.call_flag = True
        return res
    
    @api.multi
    def action_log(self):
        if self.lead_id:
            self.partner_id=self.lead_id.partner_id.id
        elif "active_id" in self._context:
            self.partner_id=self._context['active_id']
        res = super(CrmActivityLog, self).action_log()
        self.status = "done"
        #set call flag to true
        call_activity_id = self.env.ref('crm.crm_activity_data_call')
        if call_activity_id.id == self.next_activity_id.id:
            self.call_flag = True
        return res
    
    
    @api.onchange('next_activity_id')
    def onchange_next_activity_id(self):
        call = self.env.ref('crm.crm_activity_data_call')
        if call.id == self.next_activity_id.id:
            self.call_flag = True
        else:
            self.call_flag = False
    
class CrmLead(models.Model):
    _inherit="crm.lead"
    
    @api.multi
    def call_logs(self):
        call_id = self.env.ref('crm.crm_activity_data_call')
        res = []
        if call_id:
            res = [each.id for each in self.env['crm.activity.log'].search([('next_activity_id','=',call_id[0].id),('lead_id','=',self.id)])]
        
        view_id = self.env.ref('call_logs.crm_activity_log_view_form')
        tree_view_id = self.env.ref('call_logs.crm_activity_log_view_tree_call')
        return {
                'name': _('Phone Call History'),
                'view_type': 'form',
                'view_mode': 'tree,form',
                'res_model': 'crm.activity.log',
                'type': 'ir.actions.act_window',
                'domain': [('id', 'in', res)],
                'views': [(tree_view_id.id, 'tree'),(view_id.id, 'form')],
#                 'target':'new'
            }
        
    @api.multi
    def email_logs(self):
        email_id = self.env.ref('crm.crm_activity_data_email')
        res = []
        if email_id:
            res = [each.id for each in self.env['crm.activity.log'].search([('next_activity_id','=',email_id[0].id),('lead_id','=',self.id)])]
            
        view_id = self.env.ref('call_logs.crm_activity_log_view_form')
        tree_view_id = self.env.ref('call_logs.crm_activity_log_view_tree_email_task')
        return {
                'name': _('Email History'),
                'view_type': 'form',
                'view_mode': 'tree,form',
                'res_model': 'crm.activity.log',
                'type': 'ir.actions.act_window',
                'domain': [('id', 'in', res)],
                'views': [(tree_view_id.id, 'tree'),(view_id.id, 'form')],
            }
        
    @api.multi
    def task_logs(self):
        task_id = self.env.ref('crm.crm_activity_data_meeting')
        res = []
        if task_id:
            res = [each.id for each in self.env['crm.activity.log'].search([('next_activity_id','=',task_id[0].id),('lead_id','=',self.id)])]
        
        view_id = self.env.ref('call_logs.crm_activity_log_view_form')
        tree_view_id = self.env.ref('call_logs.crm_activity_log_view_tree_email_task')
        return {
                'name': _('Tasks History'),
                'view_type': 'form',
                'view_mode': 'tree,form',
                'res_model': 'crm.activity.log',
                'type': 'ir.actions.act_window',
                'domain': [('id', 'in', res)],
                'views': [(tree_view_id.id, 'tree'),(view_id.id, 'form')],
            }
        
    @api.multi
    def schedule_call(self):
        
        call_id = self.env.ref('crm.crm_activity_data_call')
        res = self.env['crm.activity.log'].search([('next_activity_id','=',call_id[0].id),('lead_id','=',self.id)])
        if not res and call_id:
            res = self.env['crm.activity.log'].create({'next_activity_id':call_id[0].id, 'flag':True,'lead_id':self.id})
        if res:
            return res[-1].action_log_and_schedule()
        
        
    
    
class ActivityCallLog(models.Model):
    _name = "activity.call.log"
    
    name=fields.Char('Name')
    description = fields.Char("Description")