# -*- coding: utf-8 -*-
{
    'name': 'Saasmate ABA file Generator',
    'version': '1.0',
    'category': 'General',
    'sequence': 3,
    'author': 'Pragmatic Techsoft Pvt. Ltd.',
    'website': 'http://pragtech.co.in',
    'description': """
 generating ABA/Cemtext/Direct Entry files used by Australian banks for bulk payments.
""",
    'depends': ['account','account_payment_batch_process'],
    'data': [
             'wizard/invoice_batch_process_view.xml',
             'wizard/aba_file_view.xml',
#              'views/account_view.xml',
    ],
    'demo': [],
    'test': [],
    'installable': True,
    'auto_install': False,
    'application': True,
}
