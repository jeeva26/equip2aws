{
    "name" : "Customer Category",
    "version" : "1.0",
    "author" : "Pragmatic Techsoft Pvt. Ltd.",
    "category": "Customer Category",
    "description": """
Adds a field customer category on partner and sale order 
========================================

""",
    "website": "www.pragtech.co.in",
    "depends" : ["base",
                 "crm",
                 "sale",
                 "sales_team"
                 ],
    "data": [
            'views/partner_view.xml',
            'views/sale_view.xml',
            'security/ir.model.access.csv'
             ],
    "installable": True,
}

