# -*- coding: utf-8 -*-
from odoo import models, fields, api
from odoo.exceptions import UserError

class industry_sector(models.Model):
    _name = "industry.sector"
    
    name = fields.Char('Customer Category')
    partner_ids = fields.One2many('res.partner','industrysector_id',)
    
    @api.multi
    def unlink(self):
        for line in self:
            if line.partner_ids:
                raise UserError(_('In order to delete a Customer Category, you must first unlink to related partner.'))
        return super(industry_sector, self).unlink()