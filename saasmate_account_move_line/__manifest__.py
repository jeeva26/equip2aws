# -*- coding: utf-8 -*-
# Copyright 2010-2013 Elico Corp. <lin.yu@elico-corp.com>
# Copyright 2017 Eficent Business and IT Consulting Services S.L.
#   (http://www.eficent.com)
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).

{
    'name': 'Saasmate Account Move Line',
    'version': '10.0.1.0.0',
    'category': 'Account',
    'summary': 'Show account move line details',
    'author': "Moses I",
    'website': 'www.ifensys.com',
    'license': 'AGPL-3',
    'depends': [
        'sale',
    ],
    'data': [
        'views/account_move_line_view.xml',
        'views/menu.xml',
    ],
    'installable': True,
    'auto_install': False,
    'application': False,
}
