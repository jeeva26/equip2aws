# -*- coding: utf-8 -*-

from odoo import api, fields, models, tools, _

class AccountMoveLine(models.Model):
    _inherit="account.move.line"
    
    user_type_id = fields.Many2one(related='account_id.user_type_id.id', store=True, readonly=True, copy=False,'Type')
    
    # It is code standard below
#     user_type_id = fields.Many2one('account.account.type', related='account_id.user_type_id', store=True, readonly=True, copy=False,'Type')
    
