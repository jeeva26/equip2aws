from odoo import fields, models, exceptions, api, _
import base64
import csv
import cStringIO
import math
from datetime import datetime
import dateutil.parser

class AccountBankStatement(models.Model):
    _inherit = 'account.bank.statement'
    
    from_import = fields.Boolean('From BS Import', default=False)
    
    def create(self, vals):
        res = super(AccountBankStatement,self).create(vals)
        if res.from_import:
            res.balance_end_real += res.balance_start
        return res
    
class AccountBankStatementImport(models.TransientModel):
    _inherit = 'account.bank.statement.import'
    
    filename = fields.Char()

    def convert_to_float(self, value):
        return float(value) if value else 0.0
    
    def check_file_type_csv(self, data_file):
        return data_file.endswith('.csv')
    
    def _parse_file(self, data_file):
        if not self.check_file_type_csv(self.filename):
            return super(AccountBankStatementImport, self)._parse_file(data_file)
        file_input = cStringIO.StringIO(data_file)
        file_input.seek(0)
        reader_info = []
        delimeter = ','
        reader = csv.reader(file_input, delimiter=delimeter,
                            lineterminator='\r\n')
        try:
            reader_info.extend(reader)
        except Exception:
#             return super(AccountBankStatementImport, self).import_file()
            raise exceptions.Warning(_("Not a valid file!"))
        data_row = False
        for count in range(0,50):
            keys = reader_info[0]
            if not isinstance(keys, list) or ('Transaction Date' not in keys or
                                              'Particulars' not in keys or 
                                              'Amount' not in keys or
                                              'Balance' not in keys ):
                del reader_info[0]
            else:
                del reader_info[0]
                data_row = True
                break
        if not data_row:
            raise exceptions.Warning(
                    _("'Date','Description','Amount' or 'Balance' keys not found"))
        values = {}
        vals_bank_statement = {}
        transactions_val = []
        total_amt = 0.00
        for i in range(len(reader_info)):
            field = reader_info[i]
            values = dict(zip(keys, field))
            date_time = dateutil.parser.parse(values['Transaction Date']).date()
            credit_debti_amt = ''
            if values['Amount']:
                amount = values['Amount']
                if amount.startswith( '-' ):
                    credit_debti_amt = values['Amount']
                else :
                    credit_debti_amt = '-' + values['Amount']
            description = str(values['Particulars'])+'-'+str(values['Code'])+'-'+str(values['Reference'])+'-'+str(values['Other Party'])
            val = {
                    'date': date_time,
                    'name': description,
                    'ref': description,
                    'amount': values['Amount'],
                    'unique_import_id': values['Transaction Date'] + credit_debti_amt + description + values['Balance'],
                    'sequence': i + 1,
                }
            
            Credit = 0.0
            Debit = 0.0
            if values['Amount']:
                amount = values['Amount']
                if amount.startswith( '-' ):
                    Credit = self.convert_to_float(values['Amount'])
                else :
                    Debit = self.convert_to_float(values['Amount'])
            
#             Debit = self.convert_to_float(values['Debit'])
            total_amt += Credit + Debit
            transactions_val.append(val)
        
        vals_bank_statement.update({
                'transactions': transactions_val,
                'balance_end_real': total_amt,
                'from_import':True
            })
        
        return None, None, [vals_bank_statement]
