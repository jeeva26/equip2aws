# -*- coding: utf-8 -*-

{
    "name": "Base Importer",
    "version": "1.0",
    "author": "Pragmatic Techsoft pvt. ltd.",
    "category": "Base",
    "description": """
Base Import
==================
Base module for all the custom migrators.

Places all custom migration modules under the Migrations menu.

""",
    "depends": [],
    'data': [
        'security/base_security.xml',
        'migrations_layout.xml',
    ],
    "demo": [],
    "test": [],
    "installable": True,
    "active": False,
}
