# -*- coding: utf-8 -*-

from openerp import models, fields, api, _
import os
from odoo.exceptions import ValidationError, Warning
import logging
_logger = logging.getLogger(__name__)

class product_image_import(models.TransientModel):
    _name = "product.image.import"

    directory_path = fields.Char(string="Directory Path")

    @api.one
    def do_image_import(self):
        """This method is used to import images from specified directory path.
        Image file name must be product Internal reference code.
        """
        if self.directory_path:
            files_in_dir = os.listdir(self.directory_path)
            count = 0
            for file in files_in_dir:
                prod = self.env['product.product'].search([('default_code','=',os.path.splitext(file)[0])])
                if prod and os.path.splitext(file)[1] != '.pdf':
                    try:
                        with open(self.directory_path + '/' + file, "rb") as f:
                            data = f.read()
                            prod.image = data.encode("base64")
                            count+=1
                            _logger.info('Done importing image for %s', (prod.name,))
                    except:
                        print file
                _logger.info('Imported %s product images', (count,)) 
        else:
            raise ValidationError(_("Directory path is not specified"))
        
        return {'type': 'ir.actions.act_window_close'}