# -*- coding: utf-8 -*-

from openerp import models, fields

class ProductVarianImportStage(models.Model):
    _name = "migration.product.variant.import.stage"

    processed = fields.Boolean(string="Processed")
    problem = fields.Char(string="Problem")
    updated_product_ids = fields.Many2many('product.product')

    internal_ref = fields.Char(string='Internal Reference')
    ean13 = fields.Char(string='EAN13 Barcode')
#     lst_price = fields.Float(string='Public Price', digits=dp.get_precision('Product Price'))
#     standard_price = fields.Float(string='Cost Price', digits=dp.get_precision('Product Price'))

    def create_or_update_variants(self,
                                  dry_run=True,
                                  ):
        """Create or update product variants"""
        
        for stage in self.filtered(lambda s: not s.processed):
            stage.write({'problem': False,
                         })

            product = self.env['product.product'].search([('default_code', '=', stage.internal_ref)])

            if not product:
                stage.write({"problem": 'Product not found: %s' % stage.internal_ref})
                continue
            if len(product) > 1:
                stage.write({"problem": 'Multiple products found: %s' % stage.internal_ref})
                continue

            variant_data = {'ean13': stage.ean13,
#                             'lst_price': stage.lst_price,
#                             'standard_price': stage.standard_price,
                            }

            if not dry_run:
                product.write(variant_data)
                stage.write({'processed': True,
                             'updated_product_ids': product.ids,
                             })
