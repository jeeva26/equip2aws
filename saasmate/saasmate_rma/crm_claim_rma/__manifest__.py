# -*- coding: utf-8 -*-
{
    'name': 'RMA Claim (Product Return Management)',
    'category': 'Generic Modules/CRM & SRM',
    'author': 'Pragmatic Techsoft Pvt. ltd.',
    'website': 'www.pragtech.co.in',
    'version': '1.0',   
    'description': """
RMA for Amtech
====================
""",               
    'depends': [
        'account',
        'purchase',
        'sale',
        'sales_team',
        'stock',
        'crm_claim_rma_code',
        'crm_rma_location',
        'product_warranty',
    ],
    'data': [
        'data/ir_sequence_type.xml',
        'data/crm_team.xml',
        'data/crm_claim_category.xml',
        'views/account_invoice.xml',
        'wizards/claim_make_picking.xml',
        'views/crm_claim.xml',
        "views/claim_line.xml",
        'views/res_partner.xml',
        'views/stock_view.xml',
        'security/ir.model.access.csv',
    ],
    'demo': [],
    'test': [
        'test/test_invoice_refund.yml'
    ],
    'installable': True,
    'auto_install': False,
}
