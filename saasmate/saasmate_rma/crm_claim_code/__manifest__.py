# -*- coding: utf-8 -*-

{
    "name": "Sequential Code for Claims",
    "version": "1.0",
    "category": "Customer Relationship Management",
    'author': 'Pragmatic Techsoft Pvt. ltd.',
    'website': 'www.pragtech.co.in',
    "contributors": [
        "Pedro M. Baeza <pedro.baeza@tecnativa.com>",
        "Ana Juaristi <ajuaristo@gmail.com>",
        "Iker Coranti <ikercoranti@avanzosc.com>",
        "Oihane Crucelaegui <oihanecrucelaegi@avanzosc.es>",
        "Alfredo de la Fuente <alfredodelafuente@avanzosc.es>",
        "Cyril Gaudin <cyril.gaudin@camptocamp.com>",
    ],
    "depends": [
        "crm_claim",
    ],
    "data": [
        "views/crm_claim_view.xml",
        "data/claim_sequence.xml",
    ],
    'installable': True,
    "pre_init_hook": "create_code_equal_to_id",
    "post_init_hook": "assign_old_sequences",
}
