# -*- coding: utf-8 -*-

from odoo import api, fields, models
import time

# Class to create a picking in from selected return lines
class exchange_from_returned_lines(models.TransientModel):
    _name='exchange_from_returned_lines.wizard'
    _description='Wizard to create an exchange from selected return lines'
    
    exchange_line_ids = fields.Many2many('temp.exchange.line', string='Selected exchange lines')
    
    # Get selected lines to add to exchange
    def _get_selected_lines(self, cr, uid,context):
        returned_line_ids = self.pool.get('crm.claim').read(cr, uid, context['active_id'], ['return_line_ids'])['return_line_ids'] 
        returned_lines = self.pool.get('return.line').browse(cr, uid,returned_line_ids)
        M2M = []
        for line in returned_lines:
            if True: # ADD ALL LINE line.selected:
                M2M.append(self.pool.get('temp.exchange.line').create(cr, uid, {
					    'name' : "none",
					    'returned_product_id' : line.product_id.id,
					    'returned_product_quantity' : line.product_returned_quantity,
					    'returned_prodlot_id' : line.prodlot_id.id,
					    'returned_unit_sale_price' : line.unit_sale_price,
					    'replacement_product_id': line.product_id.id,
					    'replacement_product_quantity' : line.product_returned_quantity,
				    }))
        return M2M    
   
    _defaults = {
        'exchange_line_ids': _get_selected_lines,
    }    

    # If "Cancel" button pressed
    @api.one
    def action_cancel(self):
        return {'type': 'ir.actions.act_window_close',}

    # If "Create" button pressed
    def action_create_exchange(self, cr, uid, ids, context=None):
        for exchange in self.browse(cr, uid,ids):
            claim_id = self.pool.get('crm.claim').browse(cr, uid, context['active_id'])
            # create exchange
            for line in exchange.exchange_line_ids:
                exchange_id = self.pool.get('product.exchange').create(cr, uid, {
					    'name' : "#",
					    'state': 'draft',
					    'exchange_send_date': time.strftime('%Y-%m-%d %H:%M:%S'),
					    'returned_product' : line.returned_product_id.id,
					    'returned_product_serial' : line.returned_prodlot_id.id,
					    'returned_product_qty' : line.returned_product_quantity,
					    'returned_unit_sale_price' : line.returned_unit_sale_price,
					    'replacement_product' : line.replacement_product_id.id,
					    'replacement_product_serial' : line.replacement_prodlot_id.id,
					    'replacement_product_qty' : line.replacement_product_quantity,
					    'claim_return_id' : claim_id.id                    
				    })

        return {'type': 'ir.actions.act_window_close',}
                              
exchange_from_returned_lines()

#===== Temp exchange line
class temp_exchange_line(models.TransientModel):
    """
    Class to handle a product exchange line
    """
    _name = "temp.exchange.line"
    _description = "List of product to exchange"
    
    name = fields.Char('Comment', size=128, help="To describe the line product problem")        
    returned_product_id = fields.Many2one('product.product', 'Returned product', required=True)
    returned_product_quantity = fields.Float('Returned quantity', digits=(12,2), help="Quantity of product returned")
    returned_unit_sale_price = fields.Float('Unit sale price', digits=(12,2),)
    returned_prodlot_id = fields.Many2one('stock.production.lot', 'Returned serial/Lot')
    replacement_product_id = fields.Many2one('product.product', 'Replacement product', required=True)
    replacement_product_quantity = fields.Float('Replacement quantity', digits=(12,2), help="Replacement quantity")
    replacement_prodlot_id = fields.Many2one('stock.production.lot', 'Replacement serial/Lot')
    
temp_exchange_line()

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
