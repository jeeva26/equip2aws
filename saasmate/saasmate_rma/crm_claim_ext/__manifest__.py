# -*- coding: utf-8 -*-

{
    'name': 'CRM claim extension',
    'version': '1.0',
    'category': 'Generic Modules/CRM & SRM', 
    'description': """
 * Add some fields to CRM claim : canal, spirit, product.exchange
 * Forbid to return product from picking out and in by masking the button

It also contain all views that weren't used when porting this module to v7.0.
Also, all wizard that were not used anymore also landed here in the wait his original
author (Pragtech Techsoft Pvt Ltd) take a decision on them.

    """,
    'author': 'Pragmatic Techsoft Pvt. ltd.',
    'website': 'www.pragtech.co.in',
    'depends': ['crm_claim'],
    'init_xml': [],
    'data': [
        'crm_claim_ext_view.xml',
        'wizard/get_empty_serial_view.xml',
#        'wizard/returned_lines_from_invoice_wizard_view.xml',
#        'wizard/picking_from_returned_lines_wizard_view.xml',
#        'wizard/refund_from_returned_lines_wizard_view.xml',
#        'wizard/exchange_from_returned_lines_wizard_view.xml',
#        'wizard/picking_from_exchange_lines_wizard_view.xml',
    ],
    'demo_xml': [], 
    'test': [], 
    'installable': True,
    'active': False,
    'certificate' : '',
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
