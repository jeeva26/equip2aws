# -*- coding: utf-8 -*-

from odoo import api, fields, models, tools, _
import logging
_logger = logging.getLogger(__name__)

class AccountInvoice(models.Model):
    _inherit = "account.invoice"
    
    def get_client_order_ref(self, origin):
        """Take origin as an argument and returns client order reference"""
        client_order_ref = self.env['sale.order'].search([('name','=',origin)], limit=1).client_order_ref
        return client_order_ref
    
    def get_ordered_product_qty(self, origin,product_id):
        """Take origin and product id as an argument and return ordered quantity"""
        order_id = self.env['sale.order'].search([('name','=',origin)], limit=1).id
        ordered_qty = self.env['sale.order.line'].search([('order_id','=',order_id),('product_id','=',product_id.id)], limit=1).product_uom_qty
        return ordered_qty    
AccountInvoice()    