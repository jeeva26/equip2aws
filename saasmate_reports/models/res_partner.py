from odoo import api,fields, models

class res_partner(models.Model):
    _inherit = 'res.partner'
    
    @api.one
    def _credit_debit_get(self):
        """This method is used to compute credit/debit of the partner on
        the basis on account move line and account type and set it on ledger amount column."""
        tables, where_clause, where_params = self.env['account.move.line']._query_get()
        self._cr.execute("""SELECT account_move_line.partner_id,act.type, SUM(account_move_line.amount_residual)
                      FROM account_move_line
                      LEFT JOIN account_account a ON (account_move_line.account_id=a.id)
                      LEFT JOIN account_account_type act ON (a.user_type_id=act.id)
                      WHERE act.type IN ('receivable','payable')
                      AND account_move_line.partner_id = %s
                      AND account_move_line.reconciled IS FALSE
                      GROUP BY account_move_line.partner_id, act.type
                      """%(self.id,))
        for pid, type, val in self._cr.fetchall():
            if type == 'receivable':
                self.ledger_amount = val
            elif type == 'payable':
                self.ledger_amount = -val
    
    ledger_amount = fields.Float(compute=_credit_debit_get,string='Ledger')

    def get_object(self,partner):
        """This method take partner as an argument and returns a partner
        browse record set."""
        partner_obj = self.browse(partner)
        return partner_obj 
    
    def get_sale_order(self,partner):
        """This method take partner as an argument and returns latest
        SO of a partner."""
        sale_order = self.env['sale.order'].search([('partner_id','=',partner)])
        if sale_order:
            return sale_order[0]  
        else:
            return False  
