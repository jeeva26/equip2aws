# -*- coding: utf-8 -*-

from odoo.exceptions import UserError
from odoo import api, fields, models, tools, _

class AccountPartnerLedger(models.TransientModel):
    _inherit = "account.report.partner.ledger"
    _description = "Account Partner Ledger"

    partner_id = fields.Many2one('res.partner',string="partner")

    @api.model
    def default_get(self, fields_list):
        """Set default partner from current record set and return the partner id."""
        res = super(AccountPartnerLedger, self).default_get(fields_list)
        context = self._context
        if context.get('active_id',False):
            res.update({'partner_id':context.get('active_id')})
        return  res 
    
    def _print_report(self, data):
        """Overridden to take a raw data and print partner ledger report by calling the super _print_report method with custom data."""
        if self.partner_id:
            data['form'].update({'partner_id': self.partner_id.id})
        data['form'].update({'reconciled': self.reconciled, 'amount_currency': self.amount_currency})
        return super(AccountPartnerLedger, self)._print_report(data)