# -*- coding: utf-8 -*-

from openerp import fields, models, _, api
from openerp.exceptions import Warning
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT

from datetime import datetime
from dateutil.relativedelta import relativedelta

class AdhocStatements(models.TransientModel):
    _name = "account.partner.statement.adhoc.wizard"
    _description = "Statement Wizard"

    description = fields.Char(string='Description')
    title = fields.Char(string='Title (for printing)')
    statement_from = fields.Date(string='Statement From', required=True, help='All reconciled transactions from this date will be included.')
    statement_date = fields.Date(string='Statement Date', required=True)
    partner_ids = fields.Many2many('res.partner', string='Included partners.')
    partner_count = fields.Integer('Number of Selected Partners', compute='count_partners')

    @api.model
    def default_get(self, fieldnames):
        result = super(AdhocStatements, self).default_get(fieldnames)
        result['statement_date'] = fields.Date.context_today(self)
        result['partner_ids'] = self.env.context.get('active_ids', [])
        return result

    @api.depends('partner_ids')
    @api.one
    def count_partners(self):
        self.partner_count = len(self.partner_ids)

    @api.multi
    def do_generation(self):

#         if self.statement_date >= fields.Date.context_today(self):
#             raise Warning(_('Statements cannot be for future.'))

        if self.statement_from > self.statement_date:
            raise Warning(_('Statement Date must be after Statement From.'))

        default_config = self.env['account.partner.statement.config'].search([('default', '=', True)])

        # Partner goes to config they are linked to, or if none, to config their payment terms go to, or if none, to "default"
        self.env.cr.execute("""
WITH ptr AS (SELECT account_payment_term_id apt_id, min(account_partner_statement_config_id) statement_config_id
    FROM account_partner_statement_config_account_payment_term_rel
    GROUP BY account_payment_term_id
    )

SELECT id, statement_config_id
FROM res_partner
WHERE statement_config_id IS NOT NULL
AND res_partner.id in %s
UNION
SELECT part.id, ptr.statement_config_id
FROM res_partner part,
    account_payment_term pt,
    ptr,
    ir_property prop
WHERE part.id in %s
    AND part.statement_config_id IS NULL
    AND prop.name='property_payment_term_id'
    AND prop.value_reference=('account.payment.term,' || pt.id::TEXT)
    AND prop.res_id=('res.partner,' || part.id::TEXT)
    AND ptr.apt_id=pt.id
    AND ptr.statement_config_id IS NOT NULL
        """, (tuple(self.partner_ids.ids),tuple(self.partner_ids.ids)))

        configs = {}
        partners_with_links = []
        for row in self.env.cr.fetchall():
            configs.setdefault(row[1], []).append(row[0])

        if default_config:
            for partner in (p for p in self.partner_ids if not p.id in partners_with_links):
                configs.setdefault(default_config[0].id, []).append(partner.id)

        batches = []
        for config_id, partner_ids in configs.items():
            batches.append(self.env['account.partner.statement.batch'].create_batch(config_id,
                                                                                    'adhoc',
                                                                                    {'description': self.description,
                                                                                     'title': self.title,
                                                                                     },
                                                                         self.statement_from,
                                                                         self.statement_date,
                                                                         partner_ids,
                                                                         ignore_overlap=True
                                                                         )
                           )
        if len(batches) == 1:
            return batches[0].view_batch_statements()

        return {'type': 'ir.actions.act_window_close'}

    @api.multi
    @api.onchange('statement_from')
    def onchange_date_from(self):
        result = {}
        if self.statement_from:
            self.generate_name_and_title()
        return result

    @api.multi
    @api.onchange('statement_date')
    def onchange_date_to(self):
        result = {}
        if self.statement_date:
            self.generate_name_and_title()
        return result

    def generate_name_and_title(self):
        if self.statement_from and self.statement_date:
            self.title = 'Statement for %s' % datetime.strptime(self.statement_date, DEFAULT_SERVER_DATE_FORMAT).strftime('%e %b %Y')
            overlap_count = len(self.env['account.partner.statement.batch'].search([('type', '=', 'adhoc'), ('statement_date', '=', self.statement_date)]))
            self.description = 'Ad hoc on %s%s' % (self.statement_date, overlap_count and ' (%s)' % (overlap_count + 1) or '')
