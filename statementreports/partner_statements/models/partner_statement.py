# -*- coding: utf-8 -*-


"""
Note: when we convert this to work for currency, we need to revert our thinking.

The currency for the statement should come from the partner's sale pricelist.

Then, every transaction should be converted to this.

This currency may be the same as the company's, or different, or may not even appear on the transaction

(e.g. Partner is in Euro, our company in AUD, and the invoice in USD!)

However, we would expect most transactions to be in company or partner currency, in which case the 
correct values should be stored in the account move or in the reconcile or somewhere...
"""



from openerp import models, fields, _, api
from openerp.exceptions import Warning

import openerp.addons.decimal_precision as dp

from datetime import datetime
from dateutil.relativedelta import relativedelta
from lxml import etree
import pytz

import json

def date_to_utc_timestamp(instance, date):
    timestamp = fields.Datetime.from_string(fields.Date.to_string(date) + ' 00:00:00')
    tz_name = instance._context.get('tz') or instance.env.user.tz
    if tz_name:
        timestamp = pytz.timezone(tz_name).localize(timestamp)
    else:
        timestamp = pytz.utc.localize(timestamp, is_dst=False)  # UTC = no DST
    return timestamp.astimezone(pytz.utc)

class PartnerStatementConfig(models.Model):
    _inherit = "res.partner"

    statement_config_id = fields.Many2one('account.partner.statement.config', string='Overriding Statement Cycle', ondelete='set null')

class PartnerStatmentConfig(models.Model):
    _name = 'account.partner.statement.config'
    _description = 'Account Partner Statement Configuration'

    name = fields.Char(string='Description', size=60, required=True)
    periods = fields.Integer(string='Number of Statement Periods (1-5)', required=True)
    calc_type = fields.Selection([('monthly','Monthly Statements'),('weekly','Weekly Statements'),('periodic','Periodic Statements')], string='Calculation', required=True,
                                 help='Monthly Statements are aged month by month, on the day specified.\nWeekly Statements are aged week by week, on the day specified.\nPeriod Statements age by the period length specified.')
    day_of_month = fields.Integer(string='Day of Month', help='Day of Month that periodic values are to be aged (31 for last day of month).')
    day_of_week = fields.Selection([('0', 'Monday'), ('1', 'Tuesday'), ('3', 'Wednesday'), ('4', 'Thursday'), ('5', 'Friday'), ('6', 'Saturday'), ('7', 'Sunday')], string='Day of Week', help='Day of Week that periodic values are to be aged.')
    period_length = fields.Integer(string='Period Length', help='Length of period calculations for periodically run reports.')
    future_title = fields.Char(string='Future Title', size=20)
    current_title = fields.Char(string='Current Title', size=20)
    period_title_1 = fields.Char(string='Period 1 Title', size=20)
    period_title_2 = fields.Char(string='Period 2 Title', size=20)
    period_title_3 = fields.Char(string='Period 3 Title', size=20)
    period_title_4 = fields.Char(string='Period 4 Title', size=20)
    period_title_5 = fields.Char(string='Period 5 Title', size=20)
    default = fields.Boolean(string='Default Statements')
    payment_term_ids = fields.Many2many('account.payment.term', string='Linked Payment Terms')
    partner_ids = fields.One2many('res.partner', 'statement_config_id', string='Linked Partners')

    @api.one
    @api.constrains('periods')
    def _check_periods(self):
        if not (1 <= self.periods <= 5):
            raise Warning(_('Periods must be between 1 and 5 for %s.') % self.name)

    @api.multi
    @api.onchange('calc_type', 'day_of_month', 'day_of_week', 'period_length')
    def onchange_calc(self):
        if self.calc_type == 'monthly' and not (1 <= self.day_of_month <= 31):
            self.day_of_month = 31
        if self.calc_type == 'periodic' and self.period_length <= 0:
            self.period_length = 30

    @api.multi
    @api.onchange('periods')
    def onchange_periods(self):
        if not 1 <= self.periods <= 5:
            raise Warning(_("Periods must be between 1 and 5."))

    def one_period_back(self, calc_date):
        if self.calc_type == 'monthly':
            previous_date = calc_date + relativedelta(months=-1, day=self.day_of_month)
        elif self.calc_type == 'weekly':
            previous_date = calc_date + relativedelta(weeks=-1)
        else:
            previous_date = calc_date - relativedelta(days=self.period_length)
        return previous_date

    def one_period_forward(self, calc_date):
        if self.calc_type == 'monthly':
            next_date = calc_date + relativedelta(months=1, day=self.day_of_month)
        elif self.calc_type == 'weekly':
            next_date = calc_date + relativedelta(weeks=1)
        else:
            next_date = calc_date + relativedelta(days=self.period_length)
        return next_date

    def calculated_date(self, statement_from):
        if self.calc_type == 'monthly':
            if statement_from.day > self.day_of_month:
                statement_date = statement_from + relativedelta(months=1, day=self.day_of_month)
            else:
                statement_date = statement_from + relativedelta(day=self.day_of_month)
        elif self.calc_type == 'weekly':
            statement_date = statement_from + relativedelta(weekday=int(self.day_of_week))
        else:
            statement_date = statement_from + relativedelta(days=self.period_length - 1)
        return statement_date

    def next_date(self, calc_date):
        statement_from = calc_date + relativedelta(days=1)
        return statement_from, self.calculated_date(statement_from)

    def first_date(self):
        today =  fields.Date.from_string(fields.Date.context_today(self))
        if self.calc_type == 'monthly':
            if today.day > self.day_of_month:
                statement_date = today + relativedelta(day=self.day_of_month)
            else:
                statement_date = today + relativedelta(months=-1, day=self.day_of_month)
        elif self.calc_type == 'weekly':
            statement_date = today + relativedelta(weeks=-1, weekday=int(self.day_of_week))
        else:
            statement_date = today - relativedelta(days=1)
        return self.one_period_back(statement_date) + relativedelta(days=1), statement_date


class PartnerStatementBatch(models.Model):
    _name = 'account.partner.statement.batch'
    _description = 'Account Partner Statement Batch'

    @api.multi
    def name_get(self):
        return [(batch.id, '%s on %s' % (batch.config_id.name, batch.statement_date)) for batch in self]

    type = fields.Selection([('generated','Generated'),('adhoc','Ad hoc')], default='generated', required=True, string='Batch Type')
    description = fields.Char(string='Description')
    title = fields.Char(string='Title')
    run_at = fields.Datetime(string='Batch Created', readonly=True)
    config_id = fields.Many2one('account.partner.statement.config', string='Type')
    statement_date = fields.Date(string='Statement Date', help="Transactions due on or before this are counted as overdue.")
    statement_from = fields.Date(string='Statement From')
    cut_off_future = fields.Date(string='Cut Off Future', help="Transactions due on or before this are counted as current due, or overdue.  If they are after this date, they are future due.")
    cut_off_1 = fields.Date(string='Cut Off 1', help="Transactions due on or before this are counted as 2 periods overdue.")
    cut_off_2 = fields.Date(string='Cut Off 2', help="Transactions due on or before this are counted as 3 periods overdue.")
    cut_off_3 = fields.Date(string='Cut Off 3', help="Transactions due on or before this are counted as 4 periods overdue.")
    cut_off_4 = fields.Date(string='Cut Off 4', help="Transactions due on or before this are counted as 5 periods overdue.")
    user_id = fields.Many2one('res.users', string='Created by', readonly=True)
    statement_ids = fields.One2many('account.partner.statement.header', 'batch_id', string='Statements')

    # In reality, ultimately there should be a statement per partner per currency... At the moment, no company
    # and assumptions are straightforward about currency...


    def create_batch(self, config_id, b_type, values, statement_from, statement_date, partner_ids, ignore_overlap=False):

        partner_statements_done = []
        if not ignore_overlap:
            overlap_batches = self.env['account.partner.statement.batch'].search(['&', '&', ('type', '=', b_type), ('statement_from', '<=', statement_date), ('statement_date', '>=', statement_from)])
            if overlap_batches:
                self.env.cr.execute("""
SELECT partner_id, company_id, currency_id
FROM account_partner_statement_header
WHERE partner_id IN %s
AND batch_id IN %s
                """, (tuple(partner_ids), tuple(overlap_batches.ids))
                )
                for res in self.env.cr.fetchall():
                    partner_statements_done.append((res[0], res[1], res[2]))

        config = self.env['account.partner.statement.config'].browse(config_id)

        d_statement_from = fields.Date.from_string(statement_from)
        d_statement_date = fields.Date.from_string(statement_date)
        d_sdf = config.one_period_forward(d_statement_date)
        d_sd1 = config.one_period_back(d_statement_date)
        d_sd2 = config.one_period_back(d_sd1)
        d_sd3 = config.one_period_back(d_sd2)
        d_sd4 = config.one_period_back(d_sd3)

        values.update({'type': b_type,
                       'run_at': datetime.now(),
                       'config_id': config.id,
                       'statement_from': statement_from,
                       'statement_date': statement_date,
                       'cut_off_future': fields.Date.to_string(d_sdf) or False,
                       'cut_off_1': config.periods >= 2 and fields.Date.to_string(d_sd1) or False,
                       'cut_off_2': config.periods >= 3 and fields.Date.to_string(d_sd2) or False,
                       'cut_off_3': config.periods >= 4 and fields.Date.to_string(d_sd3) or False,
                       'cut_off_4': config.periods >= 5 and fields.Date.to_string(d_sd4) or False,
                       'user_id': self.env.uid,
                       })

        batch = self.create(values)
        partner_statements = {}

        # Opening balance transactions
        transactions = self.transactions_as_at(d_statement_from - relativedelta(days=1), partner_ids)
        for row in transactions:
	   statement_header = False
 
           pcc = (row['partner_id'], row['company_id'], row['currency_id'])
           if not pcc in partner_statements_done:
                if not pcc in partner_statements:
                    partner_statements[pcc] = self.env['account.partner.statement.header'].empty_header(batch, pcc)
                statement_header = partner_statements[pcc]
		if statement_header:
                    is_credit = row['credit'] and True or False
                    vals = {'statement_id': statement_header.id,
                        'position': 'opening',
                        'sequence': len(statement_header.ob_statement_line_ids) + len(statement_header.cb_statement_line_ids),
                        'move_line_id': row['aml_id'],
                        'is_credit': is_credit,
                        'debit': not is_credit and row['debit'] or 0,
                        'credit': is_credit and (row['credit'] - row['debit_applied']) or 0, # for credits, only store balance - see notes below
                        'credit_applied': not is_credit and row['credit_applied'] or 0,
                        'debit_applied': 0,
                        }
                    vals.update(self.transaction_details(row['aml_id']))
                    new_bal_id = self.env['account.partner.statement.bal'].create(vals)
                    if not is_credit:
                        applied_transactions = self.applied_transactions_as_at(d_statement_from - relativedelta(days=1), row['aml_id'])
                        for app_row in applied_transactions:
                            vals = {'statement_id': statement_header.id,
                                'position': 'opening',
                                'sequence': len(statement_header.ob_statement_line_ids) + len(statement_header.cb_statement_line_ids),
                                'move_line_id': app_row['aml_id'],
                                'applied_debit_id': new_bal_id.id,
                                'is_credit': True,
                                'debit': 0,
                                'credit': app_row['credit'],
                                'credit_applied': 0,
                                'debit_applied': app_row['credit'],
                                }
                            vals.update(self.transaction_details(app_row['aml_id']))
                            app_bal_id = self.env['account.partner.statement.bal'].create(vals)
                    if row['currency_id'] == row['co_currency_id']:
                        statement_header.start_due += new_bal_id.balance
                    else:
                        # have to do this in currency...
                        statement_header.start_due += 0

        # Closing balance transactions
        transactions = self.transactions_as_at(d_statement_date, partner_ids)
        for row in transactions:
	    statement_header = False
            pcc = (row['partner_id'], row['company_id'], row['currency_id'])
            if not pcc in partner_statements_done:
                if not pcc in partner_statements:
                    partner_statements[pcc] = self.env['account.partner.statement.header'].empty_header(batch, pcc)
                statement_header = partner_statements[pcc]
		if statement_header:
                    is_credit = row['credit'] and True or False
                    vals = {'statement_id': statement_header.id,
                        'position': 'closing',
                        'sequence': len(statement_header.ob_statement_line_ids) + len(statement_header.cb_statement_line_ids),
                        'move_line_id': row['aml_id'],
                        'is_credit': is_credit,
                        'debit': not is_credit and row['debit'] or 0,
                        'credit': is_credit and (row['credit'] - row['debit_applied']) or 0, # for credits, only store balance - see notes below
                        'credit_applied': not is_credit and row['credit_applied'] or 0,
                        'debit_applied': 0,
                        }
                    vals.update(self.transaction_details(row['aml_id']))
                    new_bal_id = self.env['account.partner.statement.bal'].create(vals)
                    if not is_credit:
                        applied_transactions = self.applied_transactions_as_at(d_statement_date, row['aml_id'])
                        for app_row in applied_transactions:
                            vals = {'statement_id': statement_header.id,
                                'position': 'closing',
                                'sequence': len(statement_header.ob_statement_line_ids) + len(statement_header.cb_statement_line_ids),
                                'move_line_id': app_row['aml_id'],
                                'applied_debit_id': new_bal_id.id,
                                'is_credit': True,
                                'debit': 0,
                                'credit': app_row['credit'],
                                'credit_applied': 0,
                                'debit_applied': app_row['credit'],
                                }
                            vals.update(self.transaction_details(app_row['aml_id']))
                            app_bal_id = self.env['account.partner.statement.bal'].create(vals)
                    if row['currency_id'] == row['co_currency_id']:
                        statement_header.total_due += new_bal_id.balance
                    else:
                        # have to do this in currency...
                        statement_header.total_due += 0

        # Period transactions
        transactions = self.transactions_between(d_statement_from, d_statement_date, partner_ids)
        for row in transactions:
	    statement_header = False
            pcc = (row['partner_id'], row['company_id'], row['currency_id'])
            if not pcc in partner_statements_done:
                if not pcc in partner_statements:
                    partner_statements[pcc] = self.env['account.partner.statement.header'].empty_header(batch, pcc)
                statement_header = partner_statements[pcc]
		if statement_header:
                    is_credit = row['credit'] and True or False
                    vals = {'statement_id': statement_header.id,
                        'sequence': len(statement_header.statement_line_ids),
                        'move_line_id': row['aml_id'],
                        'is_credit': is_credit,
                        'debit': not is_credit and row['debit'] or 0,
                        'credit': is_credit and row['credit'] or 0,
                        }
                    vals.update(self.transaction_details(row['aml_id']))
                    new_line_id = self.env['account.partner.statement.line'].create(vals)
	    if statement_header:
                if row['currency_id'] == row['co_currency_id']:
                    statement_header.period_debits += new_line_id.debit
                    statement_header.period_credits += new_line_id.credit
                else:
                    # have to do this in currency...
                    statement_header.period_debits += 0
                    statement_header.period_credits += 0
                statement_header.transactions = True

        # Period transactions
        for pcc, statement_header in partner_statements.iteritems():
            balances = dict.fromkeys(range(-1, config.periods + 1), 0.0)
            oldest_debit = False

            for statement_line in statement_header.cb_statement_line_ids:
                date_due = fields.Date.from_string(statement_line.due)
                if date_due > d_sdf:
                    #future due
                    period = -1
                elif date_due > d_statement_date:
                    #current
                    period = 0
                elif config.periods <= 1 or date_due > d_sd1:
                    period = 1
                elif config.periods <= 2 or date_due > d_sd2:
                    period = 2
                elif config.periods <= 3 or date_due > d_sd3:
                    period = 3
                elif config.periods <= 4 or date_due > d_sd4:
                    period = 4
                else:
                    period = 5
                #
                statement_line.period = period
                #
                # Have to get currency correct here...
                #
                if statement_header.currency_id != statement_header.company_id.currency_id or statement_line.currency_id != statement_header.company_id.currency_id:
                    this_balance = 0
                else:
                    this_balance = statement_line.balance
                balances[period] += this_balance
                if this_balance > 0 and (not oldest_debit or statement_line.due < oldest_debit):
                    oldest_debit = statement_line.due

            statement_header.write({'now_due': statement_header.total_due - balances.get(-1, 0.0),
                                    'future_due': balances.get(-1, 0.0),
                                    'current_due': balances.get(0, 0.0),
                                    'period_1_due': balances.get(1, 0.0),
                                    'period_2_due': balances.get(2, 0.0),
                                    'period_3_due': balances.get(3, 0.0),
                                    'period_4_due': balances.get(4, 0.0),
                                    'period_5_due': balances.get(5, 0.0),
                                    'oldest_debit': oldest_debit,
                                    })
        return batch

    transactions = fields.Boolean(string='New Transactions In Period')
    start_due = fields.Float(string='Opening Balance', digits=dp.get_precision('Account'))
    now_due = fields.Float(string='Due Before Next Statement', digits=dp.get_precision('Account'))
    total_due = fields.Float(string='Total Due', digits=dp.get_precision('Account'))
    future_due = fields.Float(string='Future', digits=dp.get_precision('Account'))
    current_due = fields.Float(string='Current Due', digits=dp.get_precision('Account'))
    period_1_due = fields.Float(string='Period 1 Due', digits=dp.get_precision('Account'))
    period_2_due = fields.Float(string='Period 2 Due', digits=dp.get_precision('Account'))
    period_3_due = fields.Float(string='Period 3 Due', digits=dp.get_precision('Account'))
    period_4_due = fields.Float(string='Period 4 Due', digits=dp.get_precision('Account'))
    period_5_due = fields.Float(string='Period 5 Due', digits=dp.get_precision('Account'))
    oldest_debit = fields.Date(string='Oldest Debit Transaction')

    def transactions_as_at(self, d_as_at, partner_ids):
        # transactions which are unreconciled at the date, or reconciled after
        self.env.cr.execute("""
WITH dates AS (
    SELECT
        DATE %s date_as_at,
        TIMESTAMP %s dt_as_at
    ),
aml AS (
    SELECT
        aml.*,
        co.currency_id co_currency_id
    FROM account_move_line aml
        LEFT JOIN account_account acct ON (acct.id=aml.account_id)
        LEFT JOIN res_company co ON (co.id=aml.company_id)
        LEFT JOIN account_move am ON (am.id=aml.move_id)
        LEFT JOIN account_journal aj ON (aj.id=am.journal_id),
        dates
    WHERE
        acct.internal_type IN %s
        AND aj.type != 'situation'
        AND aml.partner_id IN %s
        AND am.state != 'draft'
        AND aml.date < dates.date_as_at
    ),
recs AS (
    SELECT
        aml.id id,
        SUM(CASE WHEN (apd.debit_move_id = aml.id AND apd.create_date < dates.dt_as_at) THEN apd.amount ELSE 0 END) debit_before,
        SUM(CASE WHEN (apc.credit_move_id = aml.id AND apc.create_date < dates.dt_as_at) THEN apc.amount ELSE 0 END) credit_before
    FROM
        aml
        LEFT JOIN account_partial_reconcile apd ON (apd.debit_move_id = aml.id)
        LEFT JOIN account_partial_reconcile apc ON (apc.credit_move_id = aml.id),
        dates
    GROUP BY aml.id
    )

SELECT
    aml.id aml_id,
    aml.partner_id,
    aml.company_id,
    COALESCE(aml.currency_id, aml.co_currency_id) AS currency_id,
    MAX(aml.co_currency_id) AS co_currency_id,
    SUM(aml.debit) AS debit,
    SUM(aml.credit) AS credit,
    SUM(recs.debit_before) AS credit_applied,
    SUM(recs.credit_before) AS debit_applied,
    -- still have to do currency...
    SUM(0) AS amount_currency
FROM aml
    LEFT JOIN recs ON (recs.id = aml.id),
    dates
WHERE
    abs((aml.debit - recs.debit_before) - (aml.credit - recs.credit_before)) >= 0.01
GROUP BY aml.id, aml.partner_id, aml.company_id, aml.currency_id, aml.co_currency_id, aml.date
ORDER BY aml.date, aml.id
        """, (fields.Date.to_string(d_as_at + relativedelta(days=1)),
              fields.Datetime.to_string(date_to_utc_timestamp(self, d_as_at + relativedelta(days=1))),
              ('receivable',), tuple(partner_ids)))
        return self.env.cr.dictfetchall()

    def applied_transactions_as_at(self, d_as_at, aml_id):
        # credit transactions which are reconciled to the passed debit move
        self.env.cr.execute("""
WITH dates AS (
    SELECT
        DATE %s date_as_at,
        TIMESTAMP %s dt_as_at
    )

SELECT
    apr.credit_move_id aml_id,
    apr.amount credit
FROM
    account_partial_reconcile apr,
    dates
WHERE
    apr.debit_move_id = %s
    AND apr.create_date < dates.dt_as_at
        """, (fields.Date.to_string(d_as_at + relativedelta(days=1)),
              fields.Datetime.to_string(date_to_utc_timestamp(self, d_as_at + relativedelta(days=1))),
              aml_id))
        return self.env.cr.dictfetchall()

    def transactions_between(self, d_from, d_as_at, partner_ids):
        self.env.cr.execute("""
WITH dates AS (
    SELECT
        DATE %s date_from,
        DATE %s date_to,
        TIMESTAMP %s dt_from,
        TIMESTAMP %s dt_to
    )

SELECT
    aml.id aml_id,
    aml.partner_id,
    aml.company_id,
    COALESCE(aml.currency_id, co.currency_id) AS currency_id,
    co.currency_id co_currency_id,
    aml.debit,
    aml.credit,
    -- still have to do currency...
    0 AS amount_currency
FROM account_move_line aml
    LEFT JOIN account_account acct ON (acct.id=aml.account_id)
    LEFT JOIN res_company co ON (co.id=aml.company_id)
    LEFT JOIN account_move am ON (am.id=aml.move_id)
    LEFT JOIN account_journal aj ON (aj.id=am.journal_id),
    dates
WHERE
    acct.internal_type IN %s
    AND aj.type != 'situation'
    AND aml.partner_id IN %s
    AND am.state != 'draft'
    AND aml.date >= dates.date_from
    AND aml.date < dates.date_to
ORDER BY aml.date, aml.id
        """, (fields.Date.to_string(d_from),
              fields.Date.to_string(d_as_at + relativedelta(days=1)),
              fields.Datetime.to_string(date_to_utc_timestamp(self, d_from)),
              fields.Datetime.to_string(date_to_utc_timestamp(self, d_as_at + relativedelta(days=1))),
              ('receivable',), tuple(partner_ids)))
        return self.env.cr.dictfetchall()

    def transaction_details(self, aml_id):
        move_line = self.env['account.move.line'].browse(aml_id)
        invoice = self.env['account.invoice'].search([('move_id', '=', move_line.move_id.id)], limit=1)
        return {'transaction_type': 'inv' if invoice.type == 'out_invoice' \
                                        else 'cred' if invoice.type == 'out_refund' \
                                        else 'pmt' if move_line.payment_id.payment_type == 'inbound' \
                                        else 'bank' if move_line.move_id.journal_id.type == 'bank' \
                                        else 'oth',
                'name': move_line.move_id.name,
                'ref': move_line.move_id.ref,
                'date': move_line.move_id.date,
                'due': invoice.date_due or move_line.move_id.date,
                'transaction_amount': move_line.debit - move_line.credit,
                'currency_id': move_line.currency_id.id or move_line.company_id.currency_id.id,
                }

    @api.multi
    def view_batch_statements(self):
        action = self.env.ref('partner_statements.action_statement')
        result = {
            'name': action.name,
            'type': action.type,
            'res_model': action.res_model,
            'view_type': action.view_type,
            'view_mode': action.view_mode,
#             'view_id': action.view_id,
            'search_view_id': action.search_view_id.id,
            'target': action.target,
            'context': "{'batch_id': %s}" % (self.id,),
            'domain': "[('batch_id', '=', %s)]" % (self.id,),
        }
        return result

class PartnerStatementHeader(models.Model):
    _name = 'account.partner.statement.header'
    _inherit = ['mail.thread']
    _description = 'Account Partner Statement Header'

    @api.multi
    def name_get(self):
        return [(s.id, '%s (%s), %s' % (s.partner_id.name, s.currency_id.name, s.statement_date)) for s in self]

    batch_id = fields.Many2one('account.partner.statement.batch', string='Batch', ondelete='cascade')
    batch_title = fields.Char(related='batch_id.title', string='Statement Title')
    statement_date = fields.Date(related='batch_id.statement_date', store=True, string='Statement Date')
    statement_from = fields.Date(related='batch_id.statement_from', string='Statement From')
    partner_id = fields.Many2one('res.partner', string='Partner')
    company_id = fields.Many2one('res.company', string='Company')
    currency_id = fields.Many2one('res.currency', string='Currency')
    partner_name = fields.Char(related='partner_id.name', store=True) # stored for sorting only
    address_id = fields.Many2one('res.partner', string='Address')
    transactions = fields.Boolean(string='New Transactions In Period')
    periods = fields.Integer(related='batch_id.config_id.periods')
    start_due = fields.Float(string='Opening Balance', digits=dp.get_precision('Account'))
    period_debits = fields.Float(string='Debits in Period', digits=dp.get_precision('Account'))
    period_credits = fields.Float(string='Credits in Period', digits=dp.get_precision('Account'))
    period_credits_neg = fields.Float(string='Credits in Period', digits=dp.get_precision('Account'), compute="_calc_neg_credits", store=False)
    now_due = fields.Float(string='Due Before Next Statement', digits=dp.get_precision('Account'))
    total_due = fields.Float(string='Total Due', digits=dp.get_precision('Account'))
    total_due_copy = fields.Float(related='total_due', string='Total Due')
    future_due = fields.Float(string='Future', digits=dp.get_precision('Account'))
    future_title = fields.Char(related='batch_id.config_id.future_title')
    current_due = fields.Float(string='Current Due', digits=dp.get_precision('Account'))
    current_title = fields.Char(related='batch_id.config_id.current_title')
    period_1_due = fields.Float(string='Period 1 Due', digits=dp.get_precision('Account'))
    period_1_title = fields.Char(related='batch_id.config_id.period_title_1')
    period_2_due = fields.Float(string='Period 2 Due', digits=dp.get_precision('Account'))
    period_2_title = fields.Char(related='batch_id.config_id.period_title_2')
    period_3_due = fields.Float(string='Period 3 Due', digits=dp.get_precision('Account'))
    period_3_title = fields.Char(related='batch_id.config_id.period_title_3')
    period_4_due = fields.Float(string='Period 4 Due', digits=dp.get_precision('Account'))
    period_4_title = fields.Char(related='batch_id.config_id.period_title_4')
    period_5_due = fields.Float(string='Period 5 Due', digits=dp.get_precision('Account'))
    period_5_title = fields.Char(related='batch_id.config_id.period_title_5')
    oldest_debit = fields.Date(string='Oldest Debit Transaction')

    ob_statement_line_ids = fields.One2many('account.partner.statement.bal', 'statement_id', string='OB Transactions', domain=[('position', '=', 'opening')])
    statement_line_ids = fields.One2many('account.partner.statement.line', 'statement_id', string='Statement Transactions')
    cb_statement_line_ids = fields.One2many('account.partner.statement.bal', 'statement_id', string='CB Transactions', domain=[('position', '=', 'closing')])

    partner_email = fields.Char(related='partner_id.email')
    partner_phone = fields.Char(related='partner_id.phone')
    partner_fax = fields.Char(related='partner_id.fax')

    _order = 'partner_name, statement_date'

    def empty_header(self, batch, pcc):
        return self.create({'batch_id': batch.id,
                            'partner_id': pcc[0],
                            'address_id': self.env['res.partner'].browse([pcc[0]]).address_get(['invoice'])['invoice'],
                            'company_id': pcc[1],
                            'currency_id': pcc[2]
                            })

    @api.one
    @api.depends('period_credits')
    def _calc_neg_credits(self):
        self.period_credits_neg = self.period_credits * -1

    @api.model
    def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
        context = self.env.context

        result = super(PartnerStatementHeader, self).fields_view_get(view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu)
        if view_type in ('tree','form') and (context.get('batch_id') or context.get('config_id')):
            config = context.get('batch_id') and self.env['account.partner.statement.batch'].browse(context['batch_id']).config_id or \
                        self.env['account.partner.statement.config'].browse(context['config_id'])

            doc = etree.fromstring(result['arch'])

            if view_type == 'tree':
                for index in range(1, 6):
                    if config.periods < index:
                        fields = doc.xpath("//field[@name='period_%s_due']" % index)
                        for field in fields:
                            field.set('invisible', '1')
                            modifiers = json.loads(field.get('modifiers', '{}'))
                            modifiers['tree_invisible'] = True
                            field.set('modifiers', json.dumps(modifiers))

            for rename in (('future_due', config.future_title),
                           ('current_due', config.current_title),
                           ('period_1_due', config.period_title_1),
                           ('period_2_due', config.period_title_2),
                           ('period_3_due', config.period_title_3),
                           ('period_4_due', config.period_title_4),
                           ('period_5_due', config.period_title_5),
                           ):
                fields = doc.xpath("//field[@name='%s']" % rename[0])
                for field in fields:
                    field.set('string', rename[1] or '')

            result['arch'] = etree.tostring(doc)

        return result

    @api.multi
    def action_statement_send(self):
        """ Open a window to compose an email, with the edi statement template
            message loaded by default
        """
        assert len(self) == 1, 'This option should only be used for a single id at a time.'
        template = self.env.ref('partner_statements.email_template_edi_statement', False)
        compose_form = self.env.ref('mail.email_compose_message_wizard_form', False)
        ctx = dict(
            default_model='account.partner.statement.header',
            default_res_id=self.id,
            default_use_template=bool(template),
            default_template_id=template.id,
            default_composition_mode='comment',
#             mark_invoice_as_sent=True,
        )
        return {
            'name': _('Compose Email'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'mail.compose.message',
            'views': [(compose_form.id, 'form')],
            'view_id': compose_form.id,
            'target': 'new',
            'context': ctx,
        }

    @api.multi
    def action_act_statement_send(self):
        """ Open a window to compose an email, with the edi statement template
            message loaded by default
        """
        assert len(self) == 1, 'This option should only be used for a single id at a time.'
        template = self.env.ref('partner_statements.email_template_edi_act_statement', False)
        compose_form = self.env.ref('mail.email_compose_message_wizard_form', False)
        ctx = dict(
            default_model='account.partner.statement.header',
            default_res_id=self.id,
            default_use_template=bool(template),
            default_template_id=template.id,
            default_composition_mode='comment',
#             mark_invoice_as_sent=True,
        )
        return {
            'name': _('Compose Email'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'mail.compose.message',
            'views': [(compose_form.id, 'form')],
            'view_id': compose_form.id,
            'target': 'new',
            'context': ctx,
        }

class PartnerStatementLine(models.Model):
    _name = 'account.partner.statement.line'
    _description = 'Account Partner Statement Lines'

    statement_id = fields.Many2one('account.partner.statement.header', string='Statement', ondelete='cascade')
    sequence = fields.Integer(string='Sequence')

    # The data in this table includes:
    #    New transactions which are still open.
    #    New transactions which are reconciled in the period.

    # Do not use the move_id for information - stored to provide a link back to the "source"
    # Do not use related, etc...
    move_line_id = fields.Many2one('account.move.line', string='Source Move', ondelete='set null')
    transaction_type = fields.Selection([('inv', 'Invoice'), ('cred', 'Credit'), ('pmt', 'Payment'), ('bank', 'EFT/Payment'), ('oth', 'Other')], string='Type')
    name = fields.Char(string='Transaction')
    ref = fields.Char(string='Reference')
    date = fields.Date(string='Transaction Date')
    due = fields.Date(string='Due Date')
    currency_id = fields.Many2one('res.currency', string='Currency')
    transaction_amount = fields.Float(string='Transaction Amount', digits=dp.get_precision('Account'))
    # "Credit" figures are expressed as credit positive.
    # All others are debit positive and credit negative:
    #    i.e. a Payment of $100 applied $70 to invoices will have:
    #            is_credit            True
    #            debit                $0
    #            credit               $100
    #            amount               -$100
    is_credit = fields.Boolean(string='Is Credit')
    debit = fields.Float(string='Debit', digits=dp.get_precision('Account'))
    credit = fields.Float(string='Credit', digits=dp.get_precision('Account'))
    amount = fields.Float(compute='_balances', digits=dp.get_precision('Account'), store=True)

    _order = 'statement_id, sequence'

    @api.one
    @api.depends('debit', 'credit')
    def _balances(self):
        """Compute the amounts in the currency of the user
        """
        self.amount = self.debit - self.credit

class PartnerStatementBal(models.Model):
    _name = 'account.partner.statement.bal'
    _description = 'Account Partner Statement Balance'

    statement_id = fields.Many2one('account.partner.statement.header', string='Statement', ondelete='cascade')
    position = fields.Selection([('opening', 'Opening'), ('closing', 'Closing')], required=True)
    sequence = fields.Integer(string='Sequence')

    # This is a version 9 table which accounts for many2manys, and works to show a nice snapshot "at a moment in time"
    #    of debits, and their credits, and unlinked credit amounts.
    # The data in this table includes:
    #    Transactions which were open at "the date" (i.e. unreconciled, or reconciled after the date).

    # Do not use the move_id for information - stored to provide a link back to the "source"
    # Do not use related, etc...

    # Debits are stored once, in full, if they have any outstanding balance.
    # Credits are store only in parts, up to the value they are used to offset debits listed, and any balance.
    # This means that a credit may be not included in full!
    move_id = fields.Many2one('account.move.line', string='Source Move', ondelete='set null')
    applied_credit_ids = fields.One2many('account.partner.statement.bal', 'applied_debit_id')
    applied_debit_id = fields.Many2one('account.partner.statement.bal')
    transaction_type = fields.Selection([('inv', 'Invoice'), ('cred', 'Credit'), ('pmt', 'Payment'), ('bank', 'EFT/Payment'), ('oth', 'Other')], string='Type')
    applied_transaction_type = fields.Selection(related=['transaction_type'], string='Applied')
    name = fields.Char(string='Transaction')
    ref = fields.Char(string='Reference')
    date = fields.Date(string='Transaction Date')
    due = fields.Date(string='Due Date')
    currency_id = fields.Many2one('res.currency', string='Currency')
    transaction_amount = fields.Float(string='Transaction Amount', digits=dp.get_precision('Account'))
    # "Credit" figures are expressed as credit positive.
    # All others are debit positive and credit negative:
    #    i.e. a Payment of $100 applied $70 to invoices will have:
    #            applied_debit_id     (link id)
    #            is_credit            True
    #            debit                $0
    #            credit               $70
    #            transaction amount   -$70
    #            credit_applied       $0
    #            debit_applied        $70
    #            applied_amount       $70
    #            balance              $0
    #    and
    #            applied_to_debit     (null)
    #            is_credit            True
    #            debit                $0
    #            credit               $30
    #            transaction amount   -$30
    #            credit_applied       $0
    #            debit_applied        $0
    #            applied_amount       $0
    #            balance              -$30
    #
    is_credit = fields.Boolean(string='Is Credit')
    debit = fields.Float(digits=dp.get_precision('Account'))
    credit = fields.Float(digits=dp.get_precision('Account'))
    amount = fields.Float(compute='_balances', digits=dp.get_precision('Account'), store=True)
    credit_applied = fields.Float(string='Credit Applied', digits=dp.get_precision('Account'))
    debit_applied = fields.Float(string='Debit Applied', digits=dp.get_precision('Account'))
    applied_amount = fields.Float(compute='_balances', string='Applied', digits=dp.get_precision('Account'), store=True)
    balance = fields.Float(compute='_balances', string='Balance', digits=dp.get_precision('Account'), store=True)
    period = fields.Integer(string='Aged Period', help='0 for current, 1 for 1 period ago, etc')

    _order = 'statement_id, sequence'

    @api.one
    @api.depends('debit', 'credit', 'credit_applied', 'debit_applied')
    def _balances(self):
        """Compute the amounts in the currency of the user
        """
        self.amount = self.debit - self.credit
        self.applied_amount = self.credit_applied - self.debit_applied
        self.balance = self.amount - self.applied_amount
