# -*- coding: utf-8 -*-
######################################################################
#
#  Note: Program metadata is available in /__init__.py
#
######################################################################

{
    "name" : "Partner Aging Analysis",
    "version" : "1.7.1",
    'author': 'Pragtech.co.in',
    'website': 'www.pragtech.com.au',
    "summary": "Aging as a view - invoices and credits",
    "description" : """
This module creates new Aged Receivable and Aged Payable views.

The default odoo Aged Partner balance report is a static PDF.
This module will give you to see this report in Analysis view.


""",
    "category": 'Accounting & Finance',
    "images" : [],
    "depends" : ["base","account_accountant"],
    "data" : [
              'views/partner_aging_supplier.xml',
              'views/partner_aging_customer.xml',
              'security/ir.model.access.csv',
    ],
    "test" : [
    ],
    "auto_install": False,
    "application": False,
    "installable": True,
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:

