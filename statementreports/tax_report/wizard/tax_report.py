# -*- coding: utf-8 -*-

import time
from datetime import datetime
from dateutil.relativedelta import relativedelta
from openerp import api, fields, models, _
from openerp.exceptions import UserError


class Accounttaxreport(models.TransientModel):

    _name = 'tax.report'
    _inherit = "account.common.account.report"
    _description = 'Tax Report'

    journal_ids = fields.Many2many('account.journal', 'account_balance_report_journal_rel', 'account_id', 'journal_id', string='Journals', required=True, default=[])
    report_type = fields.Selection([('detail', 'Detailed'), ('summary', 'Summary')], string='Report Type', required=True)

    def _print_report(self, data):
        if data['form']['date_from'] and not data['form']['date_to']:
            raise UserError(_("User error : Please Add End Date"))
        if not data['form']['date_from'] and  data['form']['date_to']:
            raise UserError(_("User error : Please Add Start Date"))
           
        data = self.pre_print_report(data)
        data['form']['report_type']=self.report_type
        records = self.env[data['model']].browse(data.get('ids', []))
        return self.env['report'].get_action(records, 'tax_report.report_tax', data=data)
    
    