# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
import pytz
from odoo import tools
from odoo import api, fields, models, _

class TaxAnalysisReport(models.Model):
    _name = "tax.analysis.report"
    _description = "Tax Analysis Report"
    _auto = False
    _rec_name = 'date_jv'

#     _columns = {
                
    date_jv = fields.Date('Journal Date', readonly=True)
    tax_id = fields.Many2one('account.tax', 'Tax', readonly=True)
    inv_id = fields.Many2one('account.invoice', 'Invoices', readonly=True)
    partner_id = fields.Many2one('res.partner', 'Partner', readonly=True)
    ref = fields.Char('# of Lines', readonly=True)
    tax_amt = fields.Float('Tax Amount', readonly=True)
    base_amt = fields.Float('Base Amount', readonly=True)
    net_amt = fields.Float('Net Amount', readonly=True)
    tax_scope = fields.Selection([('sale', 'Sales'), ('purchase', 'Purchases'), ('none', 'None')], string='Tax Scope')
    
#     }

    @api.model_cr
    def init(self):
        # self._table = sale_report
        print'tooooooooooools',tools,self._table
        tools.drop_view_if_exists(self.env.cr, self._table)
#         cr.execute("""CREATE or REPLACE VIEW %s as (
#             select min(i.id) as id,tx.name as tax_name,am.date as date,i.name as invoice,i.partner_id as partner,i.origin as ref
#             FROM account_move am
#             account_tax tx
#             left join account_invoice i on (i.move_id =am.id)
#             left join account_invoice_tax t on (t.invoice_id = i.id)
#             left join account_invoice_tax t2  on (t2.tax_id = tx.id)
#             GROUP BY tax_name,jv.date,i.name,i.partner_id,i.origin
#             
#             )""" % (self._table,))
#         cr.execute("""CREATE or REPLACE VIEW %s as (
#             select min(am.id) as id,sum(t.amount) as tax_amt,sum(i.amount_total) as net_amt,sum(i.amount_untaxed) as base_amt, t.tax_id as tax_id, am.date as date,t.invoice_id as inv_id,i.partner_id as partner_id,i.origin as ref
#             FROM account_move am
#             
#             left join account_invoice i on (i.move_id =am.id)
#             left join account_invoice_tax t on (t.invoice_id = i.id)
#             left join account_tax_filiation_rel atf on (t.tax_id = atf.parent_tax )
#             left join account_tax tx on (tx.id=t.tax_id)
#             left join account_tax_filiation_rel atf2 on (tx.id = atf2.parent_tax )
#             GROUP BY t.tax_id,am.date,t.invoice_id,i.partner_id,i.origin, t.amount
#             
#             )""" % (self._table,))

        self._cr.execute("""CREATE or REPLACE VIEW %s as (
            
            select min(am.id) as id,
            t.tax_id as tax_id,
            
            t.amount as tax_amt,
            i.amount_total as net_amt,
            i.amount_untaxed as base_amt, 
            am.date as date_jv,
            t.invoice_id as inv_id,
            i.partner_id as partner_id,
            i.origin as ref,
            tx.type_tax_use as tax_scope
            FROM account_move am
            left join account_invoice i on (i.move_id =am.id)
            left join account_invoice_line il on (il.invoice_id =i.id)
            left join account_invoice_line_tax ilt on (ilt.invoice_line_id =il.id)
            left join account_invoice_tax t on (t.invoice_id = i.id)
            left join account_tax tx on (tx.id=t.tax_id)
            GROUP BY am.id,am.date,t.invoice_id,i.partner_id,i.origin,t.tax_id,t.amount,i.amount_total,i.amount_untaxed,tx.type_tax_use
             
            )""" % (self._table,)) 
