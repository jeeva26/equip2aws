{
    "name" : "Tax Report",
    "version" : "1.0",
    "author" : "Pragmatic Techsoft Pvt Ltd",
    "category": "RAM",
    "description": """
Tax Report
========================================

""",
    "website": "http://www.pragtech.co.in",
    "depends" : ["account", "sale","account_voucher"
                 ],
    "data": [
             'report/report_layout_templates.xml',
             'report/tax_report_view.xml',
             'wizard/tax_wizard_view.xml',
             'report/report_view.xml',
             'report/tax_report_view.xml',
             'report/tax_report_pivot_view.xml',
             ],
    "installable": True,
    "active": True,
}

