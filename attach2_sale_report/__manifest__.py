# -*- coding: utf-8 -*-
{
    'name': "Attach2_sale_report",

    'summary': """
        Customized sale report format""",

    'description': """
        Format sale order report
    """,

    'author': "iFensys",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','sale'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'reports/sale_report.xml',
        'reports/report_saleorder_main.xml',
        'views/views.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}