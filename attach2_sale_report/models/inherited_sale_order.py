# -*- coding: utf-8 -*-

from odoo import api, models, fields


class SaleOrder(models.Model):
    _inherit = "sale.order"
    
    
    @api.multi
    def print_quotation(self):
        """This method is overridden to call new customized quotation report"""
        self.filtered(lambda s: s.state == 'draft').write({'state': 'sent'})
        return self.env['report'].get_action(self, 'attach2_sale_report.report_saleorder_main')