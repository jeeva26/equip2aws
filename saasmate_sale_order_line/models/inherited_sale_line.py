# -*- coding: utf-8 -*-
from odoo import api, fields, models
import odoo.addons.decimal_precision as dp
 
class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'

    margin_percentage = fields.Float(compute='_product_margin', digits=dp.get_precision('Product Price'), store=True,string='Margin(%)')
    requested_date = fields.Date('Requested Date') 
    expected_date = fields.Date('Expected Date')
    
    @api.depends('product_id', 'product_uom_qty', 'price_unit', 'price_subtotal')
    def _product_margin(self):
        for line in self:
            currency = line.order_id.pricelist_id.currency_id
            line.margin = currency.round(line.price_subtotal - ((line.purchase_price or line.product_id.standard_price) * line.product_uom_qty))
            #Added By susai
            if line.price_unit != 0:
                line.margin_percentage = currency.round((line.margin / line.price_unit ) * 100)
            else:
                line.margin_percentage = 0